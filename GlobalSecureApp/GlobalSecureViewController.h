//
//  GlobalSecureViewController.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebPageViewController.h"
#import "MainViewController.h"
#import "ContactPageViewController.h"
#import "FeedbackViewController.h"
#import "AboutViewController.h"
#import "UIImage.h"
#import "GlobalSecureAppDelegate.h"
#import "login.h"
#import "LoginViewController.h"

@interface GlobalSecureViewController : UIViewController<UITabBarControllerDelegate,UINavigationBarDelegate,UINavigationControllerDelegate>{
    
    MainViewController *mainScreenViewController;
    
}
@property (nonatomic, retain) MainViewController *mainScreenViewController;
@property (nonatomic, retain) UINavigationController *mainNavigationController;
@property (nonatomic, retain) UITabBarController *tabBarController;
@property (nonatomic, retain) UINavigationController *navController1;
@property (nonatomic, retain) UINavigationController *navController2;
@property (nonatomic, retain) UINavigationController *navController3;
@property (nonatomic, retain) UINavigationController *navController4;
-(void)mainPage;
-(void)initializeAfterViewLoad;

@end
