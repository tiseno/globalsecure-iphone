//
//  MainViewController.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import "WebPageViewController.h"
#import "LoginViewController.h"

#define kConfirmingAlertTag 0
#define kConfirmedAlertTag 1
#define kConfirmingLogoutTag 3

@interface MainViewController : UIViewController<UIApplicationDelegate,CLLocationManagerDelegate,MKMapViewDelegate,UITextFieldDelegate>{
    
    CLLocationManager *locationManager;

}

@property (retain, nonatomic) IBOutlet UIScrollView *mainScrolView;
@property (retain, nonatomic) IBOutlet UIImageView *imgMainView;
@property (retain, nonatomic) IBOutlet UILabel *lblmainViewDesc;
@property (retain, nonatomic) IBOutlet UILabel *lblballtxt;
@property (retain, nonatomic) IBOutlet UILabel *lblglobaltitle;
@property (retain, nonatomic) IBOutlet MKMapView *mapView;
@property (retain, nonatomic) NSString *memberID;

@end
