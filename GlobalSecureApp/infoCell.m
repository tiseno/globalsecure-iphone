//
//  infoCell.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "infoCell.h"

@implementation infoCell
@synthesize lblTitle, lineColor, gGSinfo;
@synthesize imageURL, fax, phone, Address, city, lblfax, lblphone, lblcity, lblname;
@synthesize lblemail, lblAddress, lblwebsite, lbloperatehour;
@synthesize website, email, operatehour;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        UILabel *glblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 13, 280, 40)];
        glblTitle.textAlignment = UITextAlignmentLeft;
        //glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        glblTitle.numberOfLines=2;
        glblTitle.textColor=[UIColor blackColor];
        glblTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
        glblTitle.backgroundColor = [UIColor clearColor];
        self.lblTitle = glblTitle;
        [glblTitle release];
        [self addSubview:lblTitle];
        
        
        UILabel *gAddress = [[UILabel alloc] initWithFrame:CGRectMake(20, 35, 220, 21)];
        gAddress.textAlignment = UITextAlignmentLeft;
        //glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        gAddress.textColor=[UIColor blackColor];
        gAddress.font = [UIFont fontWithName:@"Helvetica" size:15];
        gAddress.backgroundColor = [UIColor clearColor];
        self.Address = gAddress;
        [gAddress release];
        [self addSubview:Address];
        
       
        UIImageView *gimgpicture= [[UIImageView alloc]init];        
        //gimgsmallflag.image=[UIImage imageNamed:@"btn_more.png"];
        [gimgpicture setFrame:CGRectMake(10, 50, 40, 40)];
        self.imageURL=gimgpicture;
        [gimgpicture release];
        [self addSubview:imageURL];
        
        UILabel *gcity = [[UILabel alloc] initWithFrame:CGRectMake(20, 35, 220, 21)];
        gcity.textAlignment = UITextAlignmentLeft;
        //glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        gcity.textColor=[UIColor blackColor];
        gcity.font = [UIFont fontWithName:@"Helvetica" size:15];
        gcity.backgroundColor = [UIColor clearColor];
        self.city = gcity;
        [gcity release];
        [self addSubview:city];
        

        UILabel *glblphone = [[UILabel alloc] initWithFrame:CGRectMake(20, 35, 220, 21)];
        glblphone.textAlignment = UITextAlignmentLeft;
        //glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        glblphone.textColor=[UIColor blackColor];
        glblphone.font = [UIFont fontWithName:@"Helvetica" size:13];
        glblphone.backgroundColor = [UIColor clearColor];
        self.lblphone = glblphone;
        [glblphone release];
        [self addSubview:lblphone];
        
        UILabel *gphone = [[UILabel alloc] initWithFrame:CGRectMake(20, 35, 220, 21)];
        gphone.textAlignment = UITextAlignmentLeft;
        //glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        gphone.textColor=[UIColor blackColor];
        gphone.font = [UIFont fontWithName:@"Helvetica" size:13];
        gphone.backgroundColor = [UIColor clearColor];
        self.phone = gphone;
        [gphone release];
        [self addSubview:phone];
        
        UILabel *glblfax = [[UILabel alloc] initWithFrame:CGRectMake(20, 35, 220, 21)];
        glblfax.textAlignment = UITextAlignmentLeft;
        //glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        glblfax.textColor=[UIColor blackColor];
        glblfax.font = [UIFont fontWithName:@"Helvetica" size:13];
        glblfax.backgroundColor = [UIColor clearColor];
        self.lblfax = glblfax;
        [glblfax release];
        [self addSubview:lblfax];
        
        UILabel *gfax = [[UILabel alloc] initWithFrame:CGRectMake(20, 35, 220, 21)];
        gfax.textAlignment = UITextAlignmentLeft;
        //glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        gfax.textColor=[UIColor blackColor];
        gfax.font = [UIFont fontWithName:@"Helvetica" size:13];
        gfax.backgroundColor = [UIColor clearColor];
        self.fax = gfax;
        [gfax release];
        [self addSubview:fax];
        
        UILabel *gwebsite = [[UILabel alloc] initWithFrame:CGRectMake(20, 35, 220, 21)];
        gwebsite.textAlignment = UITextAlignmentLeft;
        //glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        gwebsite.textColor=[UIColor blackColor];
        gwebsite.font = [UIFont fontWithName:@"Helvetica" size:13];
        gwebsite.backgroundColor = [UIColor clearColor];
        self.website = gwebsite;
        [gwebsite release];
        [self addSubview:website];
        
        UILabel *gemail = [[UILabel alloc] initWithFrame:CGRectMake(20, 35, 220, 21)];
        gemail.textAlignment = UITextAlignmentLeft;
        //glblTitle.textColor = [UIColor colorWithRed:(160/255.0) green:(1/255.0) blue:(4/255.0) alpha:1.0];
        gemail.textColor=[UIColor blackColor];
        gemail.font = [UIFont fontWithName:@"Helvetica" size:13];
        gemail.backgroundColor = [UIColor clearColor];
        self.email = gemail;
        [gemail release];
        [self addSubview:email];
        

        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)dealloc
{


    [lineColor release];
    [imageURL release];
    [lblTitle release];
    [super dealloc];
}

@end
