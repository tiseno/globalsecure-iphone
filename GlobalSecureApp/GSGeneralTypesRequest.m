//
//  GSGeneralTypesRequest.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GSGeneralTypesRequest.h"

@implementation GSGeneralTypesRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"getGeneralType";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{
    return @"";
}

@end
