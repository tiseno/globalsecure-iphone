//
//  GSGeneralTypesResponse.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GSGeneralTypesResponse.h"

@implementation GSGeneralTypesResponse
@synthesize GeneralTypesArr;

-(void)dealloc
{
    [GeneralTypesArr release];
    [super dealloc];
}

@end
