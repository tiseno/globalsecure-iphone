//
//  GeneralTypes.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GeneralTypes.h"

@implementation GeneralTypes
@synthesize GeneralType;

-(void)dealloc
{
    [GeneralType release];
    [super dealloc];
}

@end
