//
//  GSinfo.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GSinfo.h"

@implementation GSinfo
@synthesize GeneralType,name,add1,add2,add3,fax,email,website,city,operatehour,imageURL,phone;

-(void)dealloc
{
    [imageURL release];
    [operatehour release];
    [email release];
    [website release];
    [fax release];
    [phone release];
    [city release];
    [add3 release];
    [add2 release];
    [add1 release];
    [name release];
    [GeneralType release];
    [super dealloc];
}

@end


/* <GeneralType>string</GeneralType>
 <name>string</name>
 <add1>string</add1>
 <add2>string</add2>
 <add3>string</add3>
 <city>string</city>
 <phone>string</phone>
 <fax>string</fax>
 <website>string</website>
 <email>string</email>
 <operatehour>string</operatehour>
 <imageURL>string</imageURL>*/