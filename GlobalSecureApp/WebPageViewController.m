//
//  WebPageViewController.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WebPageViewController.h"

@interface WebPageViewController ()

@end

@implementation WebPageViewController
@synthesize webscrolView;
@synthesize navimgvar;
@synthesize frwbtn;
@synthesize bckbtn;
@synthesize opnsafaritapped;
@synthesize tabBarController;
@synthesize WebpdfView;
@synthesize navController1,navController2,navController3,navController4;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *urlAddress = @"http://mobilesolutions.com.my/AA/login.php";
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [WebpdfView loadRequest:requestObj];
    
    
    WebpdfView.scalesPageToFit = YES;
    
    
    [self.webscrolView addSubview:WebpdfView];
    [self.webscrolView addSubview:navimgvar];
    [self.webscrolView addSubview:frwbtn];
    [self.webscrolView addSubview:bckbtn];
    [self.webscrolView addSubview:opnsafaritapped];
    
    self.webscrolView.contentSize=CGSizeMake(self.webscrolView.frame.size.width, 460);
    
    
    UIButton* buttonLogoutTapped = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLogoutTapped.tag = 1;
    buttonLogoutTapped.frame = CGRectMake(0, 0, 55, 30);
    buttonLogoutTapped.titleLabel.text=@"Home";
    [buttonLogoutTapped setBackgroundImage:[UIImage imageNamed:@"btn_homer.png"]  forState:UIControlStateNormal];
    [buttonLogoutTapped addTarget:self action:@selector(LogoutMethod:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnLogOut = [[UIBarButtonItem alloc] initWithCustomView:buttonLogoutTapped];  

    
    NSString *backs=[[NSString alloc]initWithString:@"back"];
    self.navigationItem.title=@"Risk Control";
    self.navigationItem.leftBarButtonItem=btnLogOut;
    /*UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(10, 80, 300, 200)];
    [webView setScalesPageToFit:YES];
    //CGFloat currentLatitude = [[Order sharedOrder] location].latitude;
    //CGFloat currentLongitude = [[Order sharedOrder] location].longitude;
    //NSString *currentLocationString = [NSString stringWithFormat:@"%f,%f", currentLatitude, currentLongitude];
    //NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%@&z=15", currentLocationString]; // z - zoom
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlAddress]]];
    [self.view addSubview:webView];	
    [webView release];*/
    
    
    
    /*UIButton* buttonLogoutTapped = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLogoutTapped.tag = 1;
    buttonLogoutTapped.frame = CGRectMake(0, 0, 55, 30);
    buttonLogoutTapped.titleLabel.text=@"Home";
    [buttonLogoutTapped setBackgroundImage:[UIImage imageNamed:@"btn_edit.png"]  forState:UIControlStateNormal];
    [buttonLogoutTapped addTarget:self action:@selector(LogoutMethod:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnLogOut = [[UIBarButtonItem alloc] initWithCustomView:buttonLogoutTapped];  
    //UIBarButtonItem *toggleButton = [[UIBarButtonItem alloc] initWithTitle:@"Call" style:UIBarButtonItemStyleBordered target:roomsViewController action:@selector(callHotel:)];
    self.navigationItem.leftBarButtonItem.title = @"Log Out";
    self.navigationItem.rightBarButtonItem = btnLogOut;
    self.navigationItem.leftBarButtonItem = nil;*/
}

-(IBAction)OpenInSafari : (id) sender
{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://mobilesolutions.com.my/AA/login.php"]];    
}


-(IBAction)LogoutMethod : (id) sender
{
    
    UIButton *clicked = (UIButton *) sender;
    NSLog(@"22222%d",clicked.tag);//Here you know which button has pressed
    
}


- (void)viewDidUnload
{
    [self setWebpdfView:nil];
    [self setWebscrolView:nil];
    [self setNavimgvar:nil];
    [self setFrwbtn:nil];
    [self setBckbtn:nil];
    [self setOpnsafaritapped:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    
    [navController1 release];
    [navController2 release];
    [navController3 release];
    [navController4 release];
    [tabBarController release];
    [WebpdfView release];
    [webscrolView release];
    [navimgvar release];
    [frwbtn release];
    [bckbtn release];
    [opnsafaritapped release];
    [super dealloc];
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    return YES;
}

-(void)mainPage
{
    //self.imgLoading.hidden=YES;
    /*WebPageViewController *tmainScreenViewController=[[WebPageViewController alloc] initWithNibName:@"WebPageViewController" bundle:nil];
    mainScreenViewController.title=@"Global Secure";
    UINavigationController *navmainController=[[UINavigationController alloc] initWithRootViewController:mainScreenViewController];
    navmainController.navigationBar.tintColor=[UIColor blueColor];
    navmainController.view.frame=CGRectMake(0, 0, 320, 460);
    navmainController.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    [mainScreenViewController release];
    self.mainNavigationController=navmainController;
    [navmainController release];
    //[self.imgLoading removeFromSuperview];
    //[self.view addSubview:self.mainNavigationController.view];*/
    
    
    
    
    UITabBarController *ttabBarController = [[UITabBarController alloc] init];
    MainViewController *citiesViewController=[[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    citiesViewController.title=@"Global Secure";
    //UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:citiesViewController];
    //navController.navigationBar.tintColor=[UIColor blueColor];
    //navController.title=@"Global Secure";
    //navController.tabBarItem.image=[UIImage imageNamed:@"btn_global.png"];
    //navController.view.frame=CGRectMake(0, 0, 320, 460);
    //navController.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    
    citiesViewController.tabBarItem.image=[UIImage imageNamed:@"btn_global.png"];
    
    
    /*ContactPageViewController *promotionViewController=[[ContactPageViewController alloc] initWithNibName:@"ContactPageViewController" bundle:nil];
    promotionViewController.title=@"Global Secure";
    UINavigationController *tnavController2=[[UINavigationController alloc] initWithRootViewController:promotionViewController];
    tnavController2.navigationBar.tintColor=[UIColor blueColor];
    tnavController2.title=@"Contact";
    tnavController2.view.frame=CGRectMake(0, 0, 320, 460);
    tnavController2.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    tnavController2.tabBarItem.image=[UIImage imageNamed:@"btn_contact.png"];
    
    FeedbackViewController *reservationViewController=[[FeedbackViewController alloc] initWithNibName:@"FeedbackViewController" bundle:nil];
    reservationViewController.title=@"Global Secure";
    UINavigationController *tnavController3=[[UINavigationController alloc] initWithRootViewController:reservationViewController];
    tnavController3.navigationBar.tintColor=[UIColor blueColor];
    tnavController3.title=@"Feedback";
    tnavController3.view.frame=CGRectMake(0, 0, 320, 460);
    tnavController3.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    tnavController3.tabBarItem.image=[UIImage imageNamed:@"btn_feedback.png"];
    
    AboutViewController *aboutViewController=[[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
    aboutViewController.title=@"Global Secure";
    UINavigationController *tboutnavController3=[[UINavigationController alloc] initWithRootViewController:aboutViewController];
    tboutnavController3.navigationBar.tintColor=[UIColor blueColor];
    tboutnavController3.title=@"About";
    tboutnavController3.view.frame=CGRectMake(0, 0, 320, 460);
    tboutnavController3.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    
    tboutnavController3.tabBarItem.image=[UIImage imageNamed:@"btn_about.png"];*/
    
    /*AboutViewController *bookingViewController=[[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
     bookingViewController.title=@"My Account";
     bookingViewController.isUsedAsMyAccountViewController=YES;
     bookingViewController.tabBarItem.image=[UIImage imageNamed:@"tabbar_btn_myaccount.png"];*/
    
    NSArray *viewControllers=[NSArray arrayWithObjects:citiesViewController, nil];
    //[citiesViewController release];

    
    ttabBarController.viewControllers=viewControllers;
    ttabBarController.view.frame=CGRectMake(0, 0, 320, 460);
    self.tabBarController=ttabBarController;
    self.tabBarController.delegate=self;
    
    [ttabBarController release];
    
    
    
    
    
    [self.view addSubview:self.tabBarController.view];
    //[self.navigationController popToViewController:ttabBarController animated:YES];
    
    
    
    //self.mainScreenViewController=tmainScreenViewController;
    //[tmainScreenViewController release];
    //[self.imgLoading removeFromSuperview];
    //[self.view addSubview:self.mainNavigationController.view];
    
    //[self.view addSubview:self.mainScreenViewController.view];
    
    //[self.navigationController popToViewController:tmainScreenViewController animated:YES];
    //[];
    //[menuViewController.navigationController pushViewController:newOrderCategoryViewController animated:YES];
    
    
}




@end







