//
//  ResponseTranslator.h
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/5/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IResponseTranslator.h"
#import "GDataXMLNode.h"

#import "GeneralTypes.h"
#import "GSGeneralTypesResponse.h"
#import "GSinfo.h"
#import "getInfoResponse.h"
#import "Feedback.h"
#import "FeedbackResponse.h"

@interface ResponseTranslator : NSObject<IResponseTranslator> {
    
}

-(XMLResponse*)translate:(NSData*) xmlData;
-(XMLResponse*)translateGeneralTypes:(NSData *)xmlData;
-(XMLResponse*)translateinfo:(NSData *)xmlData;
-(XMLResponse*)translatefeedback:(NSData *)xmlData;



@end
