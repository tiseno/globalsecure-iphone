//
//  LoginViewController.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"


@implementation LoginViewController
@synthesize txtMemberID;
@synthesize rememberSwitch;
@synthesize mainScreenViewController;
@synthesize tabBarController,navController1,navController2,navController3,navController4;
@synthesize mainNavigationController,memberID;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        checkLogin=NO; 
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    GlobalSecureAppDelegate *appDelegate=[UIApplication sharedApplication].delegate;
    
    
    
    
    login *loginarr=[[login alloc]init];
    
    NSArray *items=[[NSArray alloc]init];
    items=appDelegate.loadProducts;
    
    for(login* item in items)
    {
        loginarr.login_id=item.login_id;
        loginarr.member_id=item.member_id;
        loginarr.remember_id=item.remember_id;
        
        self.txtMemberID.text=item.member_id;
        
        checkLogin=YES; 
        
        if (item.remember_id==1) {
            
            [self.rememberSwitch setOn:YES animated:YES];
        }else {
            [self.rememberSwitch setOn:NO animated:YES];
        }
    }

    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
}

- (void)viewDidUnload
{
    
    //[self setRememberSwitch:nil];
    [super viewDidUnload];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

-(IBAction)LoginTapped
{
    GlobalSecureAppDelegate *appDelegate=[UIApplication sharedApplication].delegate;
    login *loginarr=[[login alloc]init];

    if ( ![self.txtMemberID.text isEqualToString:@" "] && self.txtMemberID.text != nil) {
        
        if (rememberSwitch.on) 
        {
            loginarr.member_id=self.txtMemberID.text;
            loginarr.remember_id=1;
            
            if (checkLogin==YES) 
            {
                [appDelegate updatelogin:loginarr];
                
            }else 
            {
                [appDelegate insertlogin:loginarr]; 
            }
            
        }else 
        {
            loginarr.member_id=@" ";
            loginarr.remember_id=0;
            
            if (checkLogin==YES) 
            {
                [appDelegate updatelogin:loginarr];
                
            }else 
            {
                [appDelegate insertlogin:loginarr];
                
            }
        }
        
        NSString *IDreplace=[self.txtMemberID.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        self.memberID=IDreplace;
        [loginarr release];
        
        for (UIView* uiview in self.view.subviews) {
            [uiview removeFromSuperview];
        }
        [self mainPage];
    }else  
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter your Member ID. Thank You." message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    
}

-(void)mainPage
{
    
    //self.imgLoading.hidden=YES;
    MainViewController *tmainScreenViewController=[[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    /*//mainScreenViewController.title=@"Global Secure";
     tmainScreenViewController.title=@"Global Secure";
     UINavigationController *navmainController=[[UINavigationController alloc] initWithRootViewController:tmainScreenViewController];
     navmainController.navigationBar.tintColor=[UIColor blueColor];
     navmainController.view.frame=CGRectMake(0, 0, 320, 460);
     navmainController.navigationBar.barStyle=UIBarStyleBlackTranslucent;
     [tmainScreenViewController release];*/
    //self.mainNavigationController=navmainController;
    //[navmainController release];
    //[self.imgLoading removeFromSuperview];
    //[self.view addSubview:self.mainNavigationController.view];
    
    UIButton* buttonLogoutTapped = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLogoutTapped.tag = 1;
    buttonLogoutTapped.frame = CGRectMake(0, 0, 55, 30);
    buttonLogoutTapped.titleLabel.text=@"Home";
    [buttonLogoutTapped setBackgroundImage:[UIImage imageNamed:@"btn_homer.png"]  forState:UIControlStateNormal];
    //[buttonLogoutTapped addTarget:self action:@selector(LogoutMethod:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnLogOut = [[UIBarButtonItem alloc] initWithCustomView:buttonLogoutTapped];  
    
    
    
    
    
    UIImage *imagetopbar = [UIImage imageNamed:@"bar_top2.png"];
    
    /**/UITabBarController *ttabBarController = [[UITabBarController alloc] init];
    MainViewController *citiesViewController=[[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    citiesViewController.title=@"Global Secure";
    citiesViewController.memberID=self.memberID;
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:citiesViewController];
    navController.navigationBar.tintColor=[UIColor lightGrayColor];
    navController.title=@"Global Secure";
    
    UIImage *image = [UIImage imageNamed:@"btn_global.png"];
    UIImage *scaledImage = [image scaleToSize:CGSizeMake(80.0f, 44.0f)];    
    navController.tabBarItem.image=image;
    
    
    navController.navigationItem.backBarButtonItem = btnLogOut;
    navController.navigationItem.hidesBackButton = YES;
    navController.navigationItem.backBarButtonItem.title = @"Log Out";
    
    
    navController.view.frame=CGRectMake(0, 0, 320, 460);
    navController.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    [navController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    
    
    /*UIImage *tabBackground = [[UIImage imageNamed:@"tabBarBackground.jpg"] 
                              resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    // Set background for all UITabBars
    [[UITabBar appearance] setBackgroundImage:tabBackground];
    // Set background for only this UITabBar
    [[tabBarController tabBar] setBackgroundImage:tabBackground];*/
    
    
    
    
    ContactPageViewController *promotionViewController=[[ContactPageViewController alloc] initWithNibName:@"ContactPageViewController" bundle:nil];
    promotionViewController.title=@"Global Secure";
    promotionViewController.memberID=self.memberID;
    UINavigationController *tnavController2=[[UINavigationController alloc] initWithRootViewController:promotionViewController];
    tnavController2.navigationBar.tintColor=[UIColor lightGrayColor];
    tnavController2.title=@"Contact";
    tnavController2.view.frame=CGRectMake(0, 0, 320, 460);
    tnavController2.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    //tnavController2.tabBarItem.image=[UIImage imageNamed:@"btn_contact_lite.png"];    
    UIImage *imagecontact = [UIImage imageNamed:@"btn_contact.png"];
    UIImage *scaledImagecontact = [imagecontact scaleToSize:CGSizeMake(80.0f, 44.0f)];
    UIImage *image2contact = [UIImage imageNamed:@"btn_contact.png"];
    
    tnavController2.tabBarItem.image=image2contact;
    [tnavController2.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    
    /*UIButton* buttonAlert = [UIButton buttonWithType:UIButtonTypeCustom];
     buttonAlert.tag = 1;
     buttonAlert.frame = CGRectMake(0, 0, 55, 30);
     buttonAlert.titleLabel.text=@"Home";
     [buttonAlert setBackgroundImage:[UIImage imageNamed:@"btn_homer.png"]  forState:UIControlStateNormal];
     [buttonAlert addTarget:self action:@selector(BtnAlertTapped:) forControlEvents:UIControlEventTouchUpInside];
     
     UIBarButtonItem* btnAlert = [[UIBarButtonItem alloc] initWithCustomView:buttonAlert];  
     //self.navigationItem.title=@"Risk Control";
     self.navigationItem.leftBarButtonItem=btnAlert;*/
    
    GSinfoGeneralTypeViewController *reservationViewController=[[GSinfoGeneralTypeViewController alloc] initWithNibName:@"GSinfoGeneralTypeViewController" bundle:nil];
    reservationViewController.title=@"Global Secure";
    reservationViewController.memberID=self.memberID;
    UINavigationController *tnavController3=[[UINavigationController alloc] initWithRootViewController:reservationViewController];
    tnavController3.navigationBar.tintColor=[UIColor lightGrayColor];
    tnavController3.title=@"Reference";
    tnavController3.view.frame=CGRectMake(0, 0, 320, 460);
    tnavController3.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    UIImage *imageFeedback = [UIImage imageNamed:@"btn_feedback.png"];
    UIImage *scaledImagefeedback = [imageFeedback scaleToSize:CGSizeMake(80.0f, 44.0f)];
    tnavController3.tabBarItem.image=imageFeedback;
    [tnavController3.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    
    
    
    AboutViewController *aboutViewController=[[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
    aboutViewController.title=@"Global Secure";
    UINavigationController *tboutnavController3=[[UINavigationController alloc] initWithRootViewController:aboutViewController];
    tboutnavController3.navigationBar.tintColor=[UIColor lightGrayColor];
    tboutnavController3.title=@"About";
    aboutViewController.memberID=self.memberID;
    tboutnavController3.view.frame=CGRectMake(0, 0, 320, 460);
    tboutnavController3.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    UIImage *imageabout = [UIImage imageNamed:@"btn_about.png"];
    UIImage *scaledImageabout = [imageabout scaleToSize:CGSizeMake(80.0f, 44.0f)];
    tboutnavController3.tabBarItem.image=imageabout;
    [tboutnavController3.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    /*AboutViewController *bookingViewController=[[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
     bookingViewController.title=@"My Account";
     bookingViewController.isUsedAsMyAccountViewController=YES;
     bookingViewController.tabBarItem.image=[UIImage imageNamed:@"tabbar_btn_myaccount.png"];*/
    
    /**/NSArray *viewControllers=[NSArray arrayWithObjects:navController,tnavController2,tnavController3, tboutnavController3, nil];
    [citiesViewController release];
    self.navController1=navController;
    self.navController2=tnavController2;
    self.navController3=tnavController3;
    self.navController4=tboutnavController3;
    
    [navController release];
    [tnavController2 release];
    [tnavController3 release];
    [tboutnavController3 release];
    
    ttabBarController.viewControllers=viewControllers;
    ttabBarController.view.frame=CGRectMake(0, 0, 320, 480);
    self.tabBarController=ttabBarController;
    self.tabBarController.delegate=self;
    
    [ttabBarController release];
    
    
    //NSArray *viewControllersNavi=[NSArray arrayWithObjects:navController,tnavController2,tnavController3, tboutnavController3, nil];
    
    
    
    //[self.view addSubview:self.tabBarController.view];
    [self presentModalViewController:self.tabBarController animated:YES];
    //[self.navigationController popToViewController:ttabBarController animated:YES];
    
    
    
    //self.mainScreenViewController=tmainScreenViewController;
    //[tmainScreenViewController release];
    //[self.imgLoading removeFromSuperview];
    //[self.view addSubview:self.mainNavigationController.view];
    
    //[self.view addSubview:self.mainScreenViewController.view];
    
    //[self.view addSubview:navmainController.view];
    //[self.view addSubview:navmainController.view];
    //[self.view addSubview:self.mainScreenViewController.view];
    //[self.navigationController popToViewController:tmainScreenViewController animated:YES];
    //[];
    //[menuViewController.navigationController pushViewController:newOrderCategoryViewController animated:YES];
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    heightOfEditedView = textField.frame.size.height;
    heightOffset=textField.frame.origin.y+120;
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOffset+ heightOfEditedView), self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    
}
- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    iskeyboardDisplayed=NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [txtMemberID resignFirstResponder];
    
    return YES;
}

- (void)touchesEnded: (NSSet *)touches withEvent: (UIEvent *)event {
	for (UIView* view in self.view.subviews) {
		if ([view isKindOfClass:[UITextField class]])
			[view resignFirstResponder];
	}
}


-(void)dealloc
{
    [memberID release];
    [mainNavigationController release];
    [tabBarController release];
    [navController1 release];
    [navController2 release];
    [navController3 release];
    [navController4 release];
    [mainScreenViewController release];
    [txtMemberID release];
    [rememberSwitch release];
    [super dealloc];
}

@end
