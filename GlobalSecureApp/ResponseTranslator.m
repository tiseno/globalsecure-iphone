//
//  ResponseTranslator.m
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/5/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "ResponseTranslator.h"


@implementation ResponseTranslator
-(XMLResponse*)translate:(NSData*) xmlData
{
    XMLResponse *message=nil;
    NSError *error;
    /*if([xmlData length]!=0)
    {
        
    }*/ 
    
    GDataXMLDocument *doc =[[GDataXMLDocument alloc]initWithData:xmlData options:0 error:&error ];
    
    //NSLog(@"doc.rootElement--->%@",doc.rootElement);
    
    
    if (doc) {
        if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityGeneral"]) 
        {
            message =[self translateGeneralTypes:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityInfo"]) 
        {
            message =[self translateinfo:xmlData];
            
        }else if ([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityFeedback"]) 
        {
            message =[self translatefeedback:xmlData];
            
        }

        
        
        
        
        [doc release];
    }
    
    
	else 
	{
		NSLog(@"%@: Error decoding the document: %@", [self class], [error localizedDescription]);
	}
    return message;

}

-(XMLResponse*)translatefeedback:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    FeedbackResponse *msg =nil;
    
    if (doc) {
        NSArray *BizEntityDiscoverBali = [doc.rootElement elementsForName:@"BizEntityFeedback"];
        if (BizEntityDiscoverBali.count>0) {
            
            msg =[[[FeedbackResponse alloc] init]autorelease];
            NSMutableArray *BizEntityDiscoverBalitableArray= [[NSMutableArray alloc]initWithCapacity:BizEntityDiscoverBali.count];
            
            for (GDataXMLElement *BizEntityDiscoverBaliElemint in BizEntityDiscoverBali) {
                
                Feedback *CgetDiscoverBali =[[Feedback alloc]init];
                
                NSArray *DiscoverBaliImageIDArr =[BizEntityDiscoverBaliElemint elementsForName:@"status"];                
                if (DiscoverBaliImageIDArr.count >0) {
                    GDataXMLElement *DiscoverBaliImageIDElement =(GDataXMLElement *)[DiscoverBaliImageIDArr objectAtIndex:0];
                    CgetDiscoverBali.Status =[DiscoverBaliImageIDElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }
                
                
                [BizEntityDiscoverBalitableArray addObject:CgetDiscoverBali];
                [CgetDiscoverBali release];
            }
            msg.FeedbackArr=BizEntityDiscoverBalitableArray;
            [BizEntityDiscoverBalitableArray release];
        }
        [doc release];
    }
    return msg;
}

-(XMLResponse*)translateinfo:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    getInfoResponse *msg =nil;
    
    if (doc) {
        NSArray *BizEntityDiscoverBali = [doc.rootElement elementsForName:@"BizEntityInfo"];
        if (BizEntityDiscoverBali.count>0) {
            
            msg =[[[getInfoResponse alloc] init]autorelease];
            NSMutableArray *BizEntityDiscoverBalitableArray= [[NSMutableArray alloc]initWithCapacity:BizEntityDiscoverBali.count];
            
            for (GDataXMLElement *BizEntityDiscoverBaliElemint in BizEntityDiscoverBali) {
                
                GSinfo *CgetDiscoverBali =[[GSinfo alloc]init];
                
                NSArray *DiscoverBaliImageIDArr =[BizEntityDiscoverBaliElemint elementsForName:@"GeneralType"];                
                if (DiscoverBaliImageIDArr.count >0) {
                    GDataXMLElement *DiscoverBaliImageIDElement =(GDataXMLElement *)[DiscoverBaliImageIDArr objectAtIndex:0];
                    CgetDiscoverBali.GeneralType =[DiscoverBaliImageIDElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }
                NSArray *GeneralTypearr =[BizEntityDiscoverBaliElemint elementsForName:@"name"];                
                if (GeneralTypearr.count >0) {
                    GDataXMLElement *GeneralTypearrel =(GDataXMLElement *)[GeneralTypearr objectAtIndex:0];
                    CgetDiscoverBali.name =[GeneralTypearrel.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }
                
                NSArray *add1arr =[BizEntityDiscoverBaliElemint elementsForName:@"add1"];                
                if (add1arr.count >0) {
                    GDataXMLElement *add1arrel =(GDataXMLElement *)[add1arr objectAtIndex:0];
                    CgetDiscoverBali.add1 =[add1arrel.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }
                
                
                NSArray *add2arr =[BizEntityDiscoverBaliElemint elementsForName:@"add2"];                
                if (add2arr.count >0) {
                    GDataXMLElement *add2arrel =(GDataXMLElement *)[add2arr objectAtIndex:0];
                    CgetDiscoverBali.add2 =[add2arrel.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }

                NSArray *add3arr =[BizEntityDiscoverBaliElemint elementsForName:@"add3"];                
                if (add3arr.count >0) {
                    GDataXMLElement *add3arrel =(GDataXMLElement *)[add3arr objectAtIndex:0];
                    CgetDiscoverBali.add3 =[add3arrel.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }

                NSArray *city =[BizEntityDiscoverBaliElemint elementsForName:@"city"];                
                if (city.count >0) {
                    GDataXMLElement *cityel =(GDataXMLElement *)[city objectAtIndex:0];
                    CgetDiscoverBali.city =[cityel.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }
                
                NSArray *phone =[BizEntityDiscoverBaliElemint elementsForName:@"phone"];                
                if (phone.count >0) {
                    GDataXMLElement *phoneel =(GDataXMLElement *)[phone objectAtIndex:0];
                    CgetDiscoverBali.phone =[phoneel.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }
                
                NSArray *fax =[BizEntityDiscoverBaliElemint elementsForName:@"fax"];                
                if (fax.count >0) {
                    GDataXMLElement *efax =(GDataXMLElement *)[fax objectAtIndex:0];
                    CgetDiscoverBali.fax =[efax.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }
                
                NSArray *website =[BizEntityDiscoverBaliElemint elementsForName:@"website"];                
                if (website.count >0) {
                    GDataXMLElement *ewebsite =(GDataXMLElement *)[website objectAtIndex:0];
                    CgetDiscoverBali.website =[ewebsite.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }
                
                NSArray *email =[BizEntityDiscoverBaliElemint elementsForName:@"email"];                
                if (email.count >0) {
                    GDataXMLElement *eemail =(GDataXMLElement *)[email objectAtIndex:0];
                    CgetDiscoverBali.email =[eemail.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }
                
                NSArray *operatehour =[BizEntityDiscoverBaliElemint elementsForName:@"operatehour"];                
                if (operatehour.count >0) {
                    GDataXMLElement *eoperatehour =(GDataXMLElement *)[operatehour objectAtIndex:0];
                    CgetDiscoverBali.operatehour =[eoperatehour.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }
                
                NSArray *imageURL =[BizEntityDiscoverBaliElemint elementsForName:@"imageURL"];                
                if (imageURL.count >0) {
                    GDataXMLElement *eimageURL =(GDataXMLElement *)[imageURL objectAtIndex:0];
                    CgetDiscoverBali.imageURL =[eimageURL.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    
                }

                /* <GeneralType>string</GeneralType>
                 <name>string</name>
                 <add1>string</add1>
                 <add2>string</add2>
                 <add3>string</add3>
                 <city>string</city>
                 <phone>string</phone>
                 <fax>string</fax>
                 <website>string</website>
                 <email>string</email>
                 <operatehour>string</operatehour>
                 <imageURL>string</imageURL>*/
                
                
                [BizEntityDiscoverBalitableArray addObject:CgetDiscoverBali];
                [CgetDiscoverBali release];
            }
            msg.infoArr=BizEntityDiscoverBalitableArray;
            [BizEntityDiscoverBalitableArray release];
        }
        [doc release];
    }
    return msg;

}

-(XMLResponse*)translateGeneralTypes:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    GSGeneralTypesResponse *msg =nil;
    
    if (doc) {
        NSArray *BizEntityDiscoverBali = [doc.rootElement elementsForName:@"BizEntityGeneral"];
        if (BizEntityDiscoverBali.count>0) {
            
            msg =[[[GSGeneralTypesResponse alloc] init]autorelease];
            NSMutableArray *BizEntityDiscoverBalitableArray= [[NSMutableArray alloc]initWithCapacity:BizEntityDiscoverBali.count];
            
            for (GDataXMLElement *BizEntityDiscoverBaliElemint in BizEntityDiscoverBali) {
                
                GeneralTypes *CgetDiscoverBali =[[GeneralTypes alloc]init];
                
                NSArray *DiscoverBaliImageIDArr =[BizEntityDiscoverBaliElemint elementsForName:@"GeneralType"];                
                if (DiscoverBaliImageIDArr.count >0) {
                    GDataXMLElement *DiscoverBaliImageIDElement =(GDataXMLElement *)[DiscoverBaliImageIDArr objectAtIndex:0];
                    CgetDiscoverBali.GeneralType =[DiscoverBaliImageIDElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

                    
                }
                
                                
                [BizEntityDiscoverBalitableArray addObject:CgetDiscoverBali];
                [CgetDiscoverBali release];
            }
            msg.GeneralTypesArr=BizEntityDiscoverBalitableArray;
            [BizEntityDiscoverBalitableArray release];
        }
        [doc release];
    }
    return msg;
}



@end
