//
//  FeedbackResponse.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FeedbackResponse.h"

@implementation FeedbackResponse
@synthesize FeedbackArr;

-(void)dealloc
{
    [FeedbackArr release];
    [super dealloc];
}

@end
