//
//  MainViewController.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController
@synthesize mainScrolView;
@synthesize imgMainView;
@synthesize lblmainViewDesc;
@synthesize lblballtxt;
@synthesize lblglobaltitle;
@synthesize mapView,memberID;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"memberid--->%@",self.memberID);
    UIButton* buttonLogoutTapped = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLogoutTapped.tag = 1;
    buttonLogoutTapped.frame = CGRectMake(0, 0, 63, 32);
    buttonLogoutTapped.titleLabel.text=@"Home";
    [buttonLogoutTapped setBackgroundImage:[UIImage imageNamed:@"btn_logout.png"]  forState:UIControlStateNormal];
    [buttonLogoutTapped addTarget:self action:@selector(LogoutMethod:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnLogOut = [[UIBarButtonItem alloc] initWithCustomView:buttonLogoutTapped];  
    //UIBarButtonItem *toggleButton = [[UIBarButtonItem alloc] initWithTitle:@"Call" style:UIBarButtonItemStyleBordered target:roomsViewController action:@selector(callHotel:)];
    self.navigationItem.backBarButtonItem.title = @"Log Out";
    self.navigationItem.rightBarButtonItem = btnLogOut;
    self.navigationItem.backBarButtonItem = btnLogOut;
    
    
    UIButton* buttonAlert = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonAlert.tag = 1;
    buttonAlert.frame = CGRectMake(0, 0, 63, 32);
    buttonAlert.titleLabel.text=@"Home";
    [buttonAlert setBackgroundImage:[UIImage imageNamed:@"btn_alert.png"]  forState:UIControlStateNormal];
    [buttonAlert addTarget:self action:@selector(BtnAlertTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnAlert = [[UIBarButtonItem alloc] initWithCustomView:buttonAlert];  
    //self.navigationItem.title=@"Risk Control";
    self.navigationItem.leftBarButtonItem=btnAlert;

    
    
    
    lblmainViewDesc.text=@"Business travelers often have to work and travel to destinations that can be hostile, sensitive or even unfamiliar putting travelers, expatriates and business units at risk. \n\nThrough the branding name of Global Secure, AA International's security experts has over 17 years of experience in providing emergency security assistance along with travel risk management services and including political emergency evacuation and protection for corporate, expatriates and travelers worldwide. We look at crisis and emergency planning as an essential element in any companies' emergency response procedures.";

    //[self.mainScrolView addSubview:imgMainView];
    [self.mainScrolView addSubview:lblmainViewDesc];
    [self ScrowViewContain];
    self.mainScrolView.contentSize=CGSizeMake(self.mainScrolView.frame.size.width, 900);
}

-(void) LogoutMethod : (id) sender
{
    
    UIButton *clicked = (UIButton *) sender;
    NSLog(@"%d",clicked.tag);//Here you know which button has pressed
    //if (self.UserIDf !=nil) {
        //self.UserIDf=nil;
        
        /*PageControlClasssAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        appDelegate.userName=@"";
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"You had logged out successfully, Thank you." delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];  
        
        [alert show];  
        [alert release];
        lblBadgeCountNumbr.text=nil;
        ImgBadgeIcon.image=nil;*/
        
    //}
    
     //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://mobilesolutions.com.my/AA/login.php"]];
    
    /*WebPageViewController *tmainScreenViewController=[[WebPageViewController alloc] initWithNibName:@"WebPageViewController" bundle:nil];
    //[self.view addSubview:tmainScreenViewController.view];
    //[self.navigationController popToViewController:tmainScreenViewController animated:YES];
    //[self.view addSubview:tmainScreenViewController.view];
    [self.navigationController pushViewController:tmainScreenViewController animated:YES];
    [tmainScreenViewController release];*/
    //NSLog(@"%@",self.UserIDf);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm to Logout ?" message:@"" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=kConfirmingLogoutTag;
    [alert show];
    [alert release];
    
}

-(void) BtnAlertTapped : (id) sender
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Open in Safari." message:@"" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=kConfirmingAlertTag;
    [alert show];
    [alert release];

    
    UIButton *clicked = (UIButton *) sender;
    NSLog(@"%d",clicked.tag);//Here you know which button has pressed
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://mobilesolutions.com.my/AA/login.php"]];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://mobilesolutions.com.my/AA/login.php?memberno=GS00001SG"]];
    
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == kConfirmingAlertTag)
    {
        if(buttonIndex == 1)
        {
            
            NSString *url=[[NSString alloc]initWithFormat:@"http://mobilesolutions.com.my/AA/login.php?memberno=%@",self.memberID];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

            
            
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Cancel" message:@"The order has been deleted" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=kConfirmedAlertTag;
            [alert show];
            [alert release];*/
            
        }
    }else if(alertView.tag == kConfirmingLogoutTag) 
    {
        if(buttonIndex == 1) 
        {
            self.memberID=Nil;
            LoginViewController *GStartPageController=[[LoginViewController alloc] initWithNibName:@"LoginViewController"  bundle:nil];
            for (UIView* uiview in self.view.subviews) {
                [uiview removeFromSuperview];
            }
            [self.view addSubview:GStartPageController.view];
            [self presentModalViewController:GStartPageController animated:YES];
            
        }
        
    }
    
}




-(void)ScrowViewContain
{
    int widthOffset=0;
    int xOffset=0;
    int yOffset=0;
    
    /*UILabel *categoryLabel_up = [[UILabel alloc] initWithFrame:CGRectMake(10, 410, 200, 30)];
    categoryLabel_up.adjustsFontSizeToFitWidth = YES;
    categoryLabel_up.minimumFontSize=10;
    categoryLabel_up.textAlignment=UITextAlignmentLeft;
    categoryLabel_up.textColor = [UIColor blackColor];
    categoryLabel_up.backgroundColor = [UIColor clearColor];
    categoryLabel_up.text=@"Global Secure services include :";    
    [self.mainScrolView addSubview:categoryLabel_up];*/
    
    lblglobaltitle.frame=CGRectMake(10, 425, 270, 30); 
    lblglobaltitle.text=@"Global Secure services include :"; 
    [self.mainScrolView addSubview:lblglobaltitle];
    
    UIImage* imagehp = [UIImage imageNamed:@"icon_1.png"];
    UIImageView *imgphone=[[UIImageView alloc]initWithImage:imagehp];
    imgphone.frame=CGRectMake(10, 460, 20, 20);
    [self.mainScrolView addSubview:imgphone];
    
    UILabel *lblphone = [[UILabel alloc] initWithFrame:CGRectMake(40, 460, 190, 20)];
    lblphone.adjustsFontSizeToFitWidth = YES;
    lblphone.minimumFontSize=10;
    lblphone.textAlignment=UITextAlignmentLeft;
    lblphone.textColor = [UIColor blackColor];
    lblphone.backgroundColor = [UIColor clearColor];
    lblphone.text=@"24/7 Crisis response call centre ";    
    [self.mainScrolView addSubview:lblphone];
    
    
    UIImage* imageball = [UIImage imageNamed:@"icon_2.png"];
    UIImageView *imgball=[[UIImageView alloc]initWithImage:imageball];
    imgball.frame=CGRectMake(10, 500, 20, 20);
    [self.mainScrolView addSubview:imgball];
    
    /*UILabel *lblball = [[UILabel alloc]init];// initWithFrame:CGRectMake(40, 430, 250, 40)];
    //lblball.adjustsFontSizeToFitWidth = YES;
    lblball.minimumFontSize=1;
    lblball.numberOfLines=0;
    lblball.lineBreakMode = UILineBreakModeWordWrap;
    lblball.textAlignment=UITextAlignmentLeft;
    lblball.textColor = [UIColor blackColor];
    lblball.backgroundColor = [UIColor clearColor];
    //lblball.text=@"'Global' Travel Security Risk Management Services (TSRM) ";    
    
    CGSize maximumSize = CGSizeMake(250, 500);
    NSString *dateString = @"'Global' Travel Security Risk Management Services (TSRM) ";
    UIFont *dateFont = [UIFont fontWithName:@"Helvetica Neue" size:10];
    CGSize dateStringSize = [dateString sizeWithFont:dateFont constrainedToSize:maximumSize lineBreakMode:lblball.lineBreakMode];  
    
    
    lblball.frame = CGRectMake(40, 430, 250, dateStringSize.height); 
    lblball.text=dateString;

    
    [self.mainScrolView addSubview:lblball];*/
    
    lblballtxt.frame=CGRectMake(40, 490, 250, 40); 
    lblballtxt.text=@"'Global' Travel Security Risk Management Services (TSRM) "; 
    [self.mainScrolView addSubview:lblballtxt];
    
    
    UIImage* imagemail = [UIImage imageNamed:@"icon_3.png"];
    UIImageView *imgmail=[[UIImageView alloc]initWithImage:imagemail];
    imgmail.frame=CGRectMake(10, 538, 20, 20);
    [self.mainScrolView addSubview:imgmail];
    
    UILabel *lblmail = [[UILabel alloc] initWithFrame:CGRectMake(40, 535, 145, 20)];
    lblmail.adjustsFontSizeToFitWidth = YES;
    lblmail.minimumFontSize=10;
    lblmail.textAlignment=UITextAlignmentLeft;
    lblmail.textColor = [UIColor blackColor];
    lblmail.backgroundColor = [UIColor clearColor];
    lblmail.text=@"Email alerts and bulletins ";    
    [self.mainScrolView addSubview:lblmail];
    
    
    UIImage* imagesecure = [UIImage imageNamed:@"icon_4.png"];
    UIImageView *imgsecure=[[UIImageView alloc]initWithImage:imagesecure];
    imgsecure.frame=CGRectMake(10, 566, 20, 20);
    [self.mainScrolView addSubview:imgsecure];
    
    UILabel *lblsecure = [[UILabel alloc] initWithFrame:CGRectMake(40, 565, 280, 20)];
    lblsecure.adjustsFontSizeToFitWidth = YES;
    lblsecure.minimumFontSize=10;
    lblsecure.textAlignment=UITextAlignmentLeft;
    lblsecure.textColor = [UIColor blackColor];
    lblsecure.backgroundColor = [UIColor clearColor];
    lblsecure.text=@"Securityevacuation and crisis response capabilities ";    
    [self.mainScrolView addSubview:lblsecure];
    
    UIImage* imagepolitic = [UIImage imageNamed:@"icon_5.png"];
    UIImageView *imgpolitic=[[UIImageView alloc]initWithImage:imagepolitic];
    imgpolitic.frame=CGRectMake(10, 596, 20, 20);
    [self.mainScrolView addSubview:imgpolitic];
    
    UILabel *lblpolitic = [[UILabel alloc] initWithFrame:CGRectMake(40, 595, 150, 20)];
    lblpolitic.adjustsFontSizeToFitWidth = YES;
    lblpolitic.minimumFontSize=10;
    lblpolitic.textAlignment=UITextAlignmentLeft;
    lblpolitic.textColor = [UIColor blackColor];
    lblpolitic.backgroundColor = [UIColor clearColor];
    lblpolitic.text=@"Political and risk analysis ";    
    [self.mainScrolView addSubview:lblpolitic];
    
    UIImage* imagebell = [UIImage imageNamed:@"icon_6.png"];
    UIImageView *imgbell=[[UIImageView alloc]initWithImage:imagebell];
    imgbell.frame=CGRectMake(10, 626, 20, 20);
    [self.mainScrolView addSubview:imgbell];
    
    UILabel *lblbell = [[UILabel alloc] initWithFrame:CGRectMake(40, 625, 200, 20)];
    lblbell.adjustsFontSizeToFitWidth = YES;
    lblbell.minimumFontSize=10;
    lblbell.textAlignment=UITextAlignmentLeft;
    lblbell.textColor = [UIColor blackColor];
    lblbell.backgroundColor = [UIColor clearColor];
    lblbell.text=@"Security, risk threat assessments ";    
    [self.mainScrolView addSubview:lblbell];
    
    UIImage* imagecrisic = [UIImage imageNamed:@"icon_7.png"];
    UIImageView *imgcrisic=[[UIImageView alloc]initWithImage:imagecrisic];
    imgcrisic.frame=CGRectMake(10, 656, 20, 20);
    [self.mainScrolView addSubview:imgcrisic];
    
    UILabel *lblcrisic = [[UILabel alloc] initWithFrame:CGRectMake(40, 655, 250, 20)];
    lblcrisic.adjustsFontSizeToFitWidth = YES;
    lblcrisic.minimumFontSize=10;
    lblcrisic.textAlignment=UITextAlignmentLeft;
    lblcrisic.textColor = [UIColor blackColor];
    lblcrisic.backgroundColor = [UIColor clearColor];
    lblcrisic.text=@"Crisis management assistance and planning ";    
    [self.mainScrolView addSubview:lblcrisic];
    
    UIImage* imagetravel = [UIImage imageNamed:@"icon_8.png"];
    UIImageView *imgtravel=[[UIImageView alloc]initWithImage:imagetravel];
    imgtravel.frame=CGRectMake(10, 686, 20, 20);
    [self.mainScrolView addSubview:imgtravel];
    
    UILabel *lbltravel = [[UILabel alloc] initWithFrame:CGRectMake(40, 685, 160, 20)];
    lbltravel.adjustsFontSizeToFitWidth = YES;
    lbltravel.minimumFontSize=10;
    lbltravel.textAlignment=UITextAlignmentLeft;
    lbltravel.textColor = [UIColor blackColor];
    lbltravel.backgroundColor = [UIColor clearColor];
    lbltravel.text=@"Travel assistance services ";    
    [self.mainScrolView addSubview:lbltravel];
    
    UIImage* imageinc = [UIImage imageNamed:@"icon_9.png"];
    UIImageView *imginc=[[UIImageView alloc]initWithImage:imageinc];
    imginc.frame=CGRectMake(10, 716, 20, 20);
    [self.mainScrolView addSubview:imginc];
    
    UILabel *lblinc = [[UILabel alloc] initWithFrame:CGRectMake(40, 715, 200, 20)];
    lblinc.adjustsFontSizeToFitWidth = YES;
    lblinc.minimumFontSize=10;
    lblinc.textAlignment=UITextAlignmentLeft;
    lblinc.textColor = [UIColor blackColor];
    lblinc.backgroundColor = [UIColor clearColor];
    lblinc.text=@"Incident assistance and monitoring ";    
    [self.mainScrolView addSubview:lblinc];
    
    UIImage* imageppl = [UIImage imageNamed:@"icon_10.png"];
    UIImageView *imgppl=[[UIImageView alloc]initWithImage:imageppl];
    imgppl.frame=CGRectMake(10, 746, 20, 20);
    [self.mainScrolView addSubview:imgppl];
    
    UILabel *lblppl = [[UILabel alloc] initWithFrame:CGRectMake(40, 745, 250, 20)];
    lblppl.adjustsFontSizeToFitWidth = YES;
    lblppl.minimumFontSize=10;
    lblppl.textAlignment=UITextAlignmentLeft;
    lblppl.textColor = [UIColor blackColor];
    lblppl.backgroundColor = [UIColor clearColor];
    lblppl.text=@"Security consultancy and training services ";    
    [self.mainScrolView addSubview:lblppl];
    
    
    UIImage* imagebtl = [UIImage imageNamed:@"icon_11.png"];
    UIImageView *imgbtl=[[UIImageView alloc]initWithImage:imagebtl];
    imgbtl.frame=CGRectMake(10, 776, 20, 20);
    [self.mainScrolView addSubview:imgbtl];
    
    UILabel *lblbtl = [[UILabel alloc] initWithFrame:CGRectMake(40, 775, 115, 20)];
    lblbtl.adjustsFontSizeToFitWidth = YES;
    lblbtl.minimumFontSize=10;
    lblbtl.textAlignment=UITextAlignmentLeft;
    lblbtl.textColor = [UIColor blackColor];
    lblbtl.backgroundColor = [UIColor clearColor];
    lblbtl.text=@"Pandemic support ";    
    [self.mainScrolView addSubview:lblbtl];
    
    UIImage* imagewar = [UIImage imageNamed:@"icon_12.png"];
    UIImageView *imgwar=[[UIImageView alloc]initWithImage:imagewar];
    imgwar.frame=CGRectMake(10, 806, 20, 20);
    [self.mainScrolView addSubview:imgwar];
    
    UILabel *lblwar = [[UILabel alloc] initWithFrame:CGRectMake(40, 805, 220, 20)];
    lblwar.adjustsFontSizeToFitWidth = YES;
    lblwar.minimumFontSize=10;
    lblwar.textAlignment=UITextAlignmentLeft;
    lblwar.textColor = [UIColor blackColor];
    lblwar.backgroundColor = [UIColor clearColor];
    lblwar.text=@"Extortion, kidnap and ransom support ";    
    [self.mainScrolView addSubview:lblwar];
    
}

- (void)viewDidUnload
{
    //[self setWebpdfView:nil];
    [self setMainScrolView:nil];
    [self setImgMainView:nil];
    [self setLblmainViewDesc:nil];
    [self setLblballtxt:nil];
    [self setLblglobaltitle:nil];
    [self setMapView:nil];
    [super viewDidUnload];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    
    [mainScrolView release];
    [imgMainView release];
    [lblmainViewDesc release];
    [lblballtxt release];
    [lblglobaltitle release];
    [mapView release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.mapView.delegate = self;     
    
    locationManager = [[CLLocationManager alloc] init];

    [locationManager setDelegate:self];
    
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    
    [self.mapView setShowsUserLocation:YES];
    
    // Override point for customization after application launch.
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{    
    MKAnnotationView *annotationView = [views objectAtIndex:0];
    id<MKAnnotation> mp = [annotationView annotation];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([mp coordinate] ,550,550);
    
    [mv setRegion:region animated:YES];
}

/*- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    // If it's a relatively recent event, turn off updates to save power
    NSDate* eventDate = newLocation.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 15.0)
    {
        NSLog(@"latitude %+.6f, longitude %+.6f\n",
              newLocation.coordinate.latitude,
              newLocation.coordinate.longitude);
    }
    // else skip the event and process the next one.
}*/



@end
