//
//  WebPageViewController.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "ContactPageViewController.h"
#import "FeedbackViewController.h"
#import "AboutViewController.h"


@interface WebPageViewController : UIViewController<UIWebViewDelegate>{
    
}

@property (retain, nonatomic) IBOutlet UIWebView *WebpdfView;
@property (nonatomic, retain) UITabBarController *tabBarController;
@property (nonatomic, retain) UINavigationController *navController1;
@property (nonatomic, retain) UINavigationController *navController2;
@property (nonatomic, retain) UINavigationController *navController3;
@property (nonatomic, retain) UINavigationController *navController4;
-(void)mainPage;
@property (retain, nonatomic) IBOutlet UIScrollView *webscrolView;
@property (retain, nonatomic) IBOutlet UIImageView *navimgvar;
@property (retain, nonatomic) IBOutlet UIButton *frwbtn;
@property (retain, nonatomic) IBOutlet UIButton *bckbtn;
@property (retain, nonatomic) IBOutlet UIButton *opnsafaritapped;


@end
