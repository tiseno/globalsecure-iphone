//
//  ContactPageViewController.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ContactPageViewController.h"



@implementation ContactPageViewController
@synthesize lblname;
@synthesize lblcontact;
@synthesize lblemail;
@synthesize lblremark;
@synthesize memberID;
@synthesize fArr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton* buttonLogoutTapped = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLogoutTapped.tag = 1;
    buttonLogoutTapped.frame = CGRectMake(0, 0, 63, 32);
    buttonLogoutTapped.titleLabel.text=@"Home";
    [buttonLogoutTapped setBackgroundImage:[UIImage imageNamed:@"btn_logout.png"]  forState:UIControlStateNormal];
    [buttonLogoutTapped addTarget:self action:@selector(LogoutMethod:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnLogOut = [[UIBarButtonItem alloc] initWithCustomView:buttonLogoutTapped];  
    self.navigationItem.leftBarButtonItem.title = @"Log Out";
    self.navigationItem.rightBarButtonItem = btnLogOut;
    self.navigationItem.leftBarButtonItem = nil;
    
    
    
    UIButton* buttonAlert = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonAlert.tag = 1;
    buttonAlert.frame = CGRectMake(0, 0, 63, 32);
    buttonAlert.titleLabel.text=@"Home";
    [buttonAlert setBackgroundImage:[UIImage imageNamed:@"btn_alert.png"]  forState:UIControlStateNormal];
    [buttonAlert addTarget:self action:@selector(BtnAlertTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnAlert = [[UIBarButtonItem alloc] initWithCustomView:buttonAlert];  
    //self.navigationItem.title=@"Risk Control";
    self.navigationItem.leftBarButtonItem=btnAlert;

    
}

-(void) LogoutMethod : (id) sender
{
    
    UIButton *clicked = (UIButton *) sender;
    NSLog(@"%d",clicked.tag);//Here you know which button has pressed
    
    /*WebPageViewController *tmainScreenViewController=[[WebPageViewController alloc] initWithNibName:@"WebPageViewController" bundle:nil];
     [self.navigationController pushViewController:tmainScreenViewController animated:YES];
     [tmainScreenViewController release];*/
    //NSLog(@"%@",self.UserIDf);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm to Logout ?" message:@"" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=kConfirmingLogoutTag;
    [alert show];
    [alert release];

    
}

-(void) BtnAlertTapped : (id) sender
{
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Open in Safari." message:@"" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=kConfirmingAlertTag;
    [alert show];
    [alert release];
    
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://mobilesolutions.com.my/AA/login.php"]];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://mobilesolutions.com.my/AA/login.php?memberno=GS00001SG"]];
    
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == kConfirmingAlertTag)
    {
        if(buttonIndex == 1)
        {
            
            NSString *url=[[NSString alloc]initWithFormat:@"http://mobilesolutions.com.my/AA/login.php?memberno=%@",self.memberID];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            
            
            
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Cancel" message:@"The order has been deleted" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             alert.tag=kConfirmedAlertTag;
             [alert show];
             [alert release];*/
            
        }
    }else if(alertView.tag == kConfirmingLogoutTag) 
    {
        if(buttonIndex == 1)
        {
            self.memberID=Nil;
            LoginViewController *GStartPageController=[[LoginViewController alloc] initWithNibName:@"LoginViewController"  bundle:nil];
            for (UIView* uiview in self.view.subviews) {
                [uiview removeFromSuperview];
            }
            [self.view addSubview:GStartPageController.view];
            [self presentModalViewController:GStartPageController animated:YES];
            
        }
        
    }

    
}


-(IBAction)btncallmass
{
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:@"60376283888"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(IBAction)btncallsg
{
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:@"6568365838"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(IBAction)btnmail
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:@" "];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"enquiries.globalsecure@aa-international.com", nil];
        //NSArray *toRecipients = [NSArray arrayWithObjects:@"wong@quality-direct.net", nil];
        [mailer setToRecipients:toRecipients];
        
        //NSString *PDFpath=[[NSString alloc]initWithString:@"http://www.mobilesolutions.com.my/TUPPERWARE/AgendaSlides/Carhiremarket&Suncars_Documentation.pdf"];	
        //NSString *PDFpath=[[NSString alloc]initWithFormat:@"http://219.94.43.102/KDSOrderPDFConvertor/PDF/%@",feedBackResult];
        //NSString *emailBody = @"Have you seen the MobileTuts+ web site?";
        
        NSString *emailBody = [[NSString alloc]initWithFormat:@" "];
        [mailer setMessageBody:emailBody isHTML:NO];
        
        // only for iPad
        // mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentModalViewController:mailer animated:YES];
        
        [mailer release];
        
        //[self.loadingView removeView];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure" 
                                                        message:@"Your device doesn't support the composer sheet" 
                                                       delegate:nil 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
}

- (IBAction)openMail:(id)sender 
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:@"A Message from iOrder"];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
        [mailer setToRecipients:toRecipients];
        
        UIImage *myImage = [UIImage imageNamed:@"mobiletuts-logo.png"];
        NSData *imageData = UIImagePNGRepresentation(myImage);
        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"mobiletutsImage"];	
        
        NSString *emailBody = @"Have you seen the MobileTuts+ web site?";
        [mailer setMessageBody:emailBody isHTML:NO];
        
        // only for iPad 
        // mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentModalViewController:mailer animated:YES];
        
        [mailer release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure" 
                                                        message:@"Your device doesn't support the composer sheet" 
                                                       delegate:nil 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    
}



#pragma mark - MFMailComposeController delegate


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the Drafts folder");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send the next time the user connects to email");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail failed: the email message was nog saved or queued, possibly due to an error");
			break;
		default:
			NSLog(@"Mail not sent");
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}


- (void)viewDidUnload
{
    [self setLblname:nil];
    [self setLblcontact:nil];
    [self setLblemail:nil];
    [self setLblremark:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)lblnametapped:(id)sender
{
    lblnameViewController *glblnameViewController=[[lblnameViewController alloc] initWithNibName:@"lblnameViewController" bundle:nil];

    glblnameViewController.gContactPageViewController=self;
    [self.navigationController pushViewController:glblnameViewController animated:YES];
    [glblnameViewController release];
}

-(IBAction)lblcontacttapped:(id)sender
{
    lblContactViewController *glblnameViewController=[[lblContactViewController alloc] initWithNibName:@"lblContactViewController" bundle:nil];

    glblnameViewController.gContactPageViewController=self;
    [self.navigationController pushViewController:glblnameViewController animated:YES];
    [glblnameViewController release];
}
-(IBAction)lblemailtapped:(id)sender
{
    lblemailViewController *glblnameViewController=[[lblemailViewController alloc] initWithNibName:@"lblemailViewController" bundle:nil];

    glblnameViewController.gContactPageViewController=self;
    [self.navigationController pushViewController:glblnameViewController animated:YES];
    [glblnameViewController release];
}

-(IBAction)lblremarktapped:(id)sender
{
    lblremarkViewController *glblnameViewController=[[lblremarkViewController alloc] initWithNibName:@"lblremarkViewController" bundle:nil];
    
    glblnameViewController.gContactPageViewController=self;
    [self.navigationController pushViewController:glblnameViewController animated:YES];
    [glblnameViewController release];
}

-(IBAction)btnsubmit:(id)sender
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"You're not connected to Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        Feedback *feedbackS=[[Feedback alloc] init];
        feedbackS.Name=lblname.text;
        feedbackS.Contact=lblcontact.text;
        feedbackS.Email=lblemail.text;
        feedbackS.Remark=lblremark.text;
        
        NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
        FeedbackRequest *requestset= [[FeedbackRequest alloc] initWithsState:feedbackS];
        
        [networkHandler setDelegate:self];
        [networkHandler request:requestset];
        [requestset release];
        [networkHandler release];
        [feedbackS release];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Feedback" message:@"Thank you for your feedback." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
    
    }
}

-(BOOL)NetworkStatus
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus==NotReachable);
}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[FeedbackResponse class]])
    {
        
        
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((FeedbackResponse*)responseMessage).FeedbackArr;
        self.fArr=msgArr;
        
        for (Feedback *item in msgArr) {
            
            NSLog(@"item.GeneralType--->%@",item.Status);
            
            /**/UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Feedback" message:item.Status delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
             [alertView show];
             [alertView release];
            
            
        }
        //[tblGeneralTypes reloadData];
        
        
    }
    
}

-(void)dealloc
{
    [fArr release];
    [lblname release];
    [lblcontact release];
    [lblemail release];
    [lblremark release];
    [super dealloc];
}

@end
