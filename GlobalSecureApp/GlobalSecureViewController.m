//
//  GlobalSecureViewController.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GlobalSecureViewController.h"

@interface GlobalSecureViewController ()

@end

@implementation GlobalSecureViewController
@synthesize mainScreenViewController;
@synthesize tabBarController,navController1,navController2,navController3,navController4;
@synthesize mainNavigationController;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    GlobalSecureAppDelegate *appDelegate=[UIApplication sharedApplication].delegate;
    
    //[appDelegate insertlogin];
    //NSLog(@"appDelegate.loadProducts-->%@",appDelegate.loadProducts);
    
    
    login *loginarr=[[login alloc]init];
    
    //loginarr=appDelegate.loadProducts;
    NSArray *items=[[NSArray alloc]init];
    items=appDelegate.loadProducts;
    
    //NSLog(@"nillllitems-->%@",items);
    
    if (items!=nil) 
    {
        for(login* item in items)
        {
            loginarr.login_id=item.login_id;
            loginarr.member_id=item.member_id;
            loginarr.remember_id=item.remember_id;
            
            //NSLog(@"loginarr.member_id--->%@",loginarr.member_id);
        }

    }else {
        NSLog(@"nillll");
    }
    
        
    
    
    [self initializeAfterViewLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;//(interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)mainPage
{
    //self.imgLoading.hidden=YES;
    MainViewController *tmainScreenViewController=[[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    /*//mainScreenViewController.title=@"Global Secure";
        tmainScreenViewController.title=@"Global Secure";
     UINavigationController *navmainController=[[UINavigationController alloc] initWithRootViewController:tmainScreenViewController];
     navmainController.navigationBar.tintColor=[UIColor blueColor];
     navmainController.view.frame=CGRectMake(0, 0, 320, 460);
     navmainController.navigationBar.barStyle=UIBarStyleBlackTranslucent;
     [tmainScreenViewController release];*/
     //self.mainNavigationController=navmainController;
     //[navmainController release];
     //[self.imgLoading removeFromSuperview];
     //[self.view addSubview:self.mainNavigationController.view];
    
    UIButton* buttonLogoutTapped = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLogoutTapped.tag = 1;
    buttonLogoutTapped.frame = CGRectMake(0, 0, 55, 30);
    buttonLogoutTapped.titleLabel.text=@"Home";
    [buttonLogoutTapped setBackgroundImage:[UIImage imageNamed:@"btn_homer.png"]  forState:UIControlStateNormal];
    //[buttonLogoutTapped addTarget:self action:@selector(LogoutMethod:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnLogOut = [[UIBarButtonItem alloc] initWithCustomView:buttonLogoutTapped];  
    
    
    
    
    
    UIImage *imagetopbar = [UIImage imageNamed:@"bar_top2.png"];
    
    /**/UITabBarController *ttabBarController = [[UITabBarController alloc] init];
    MainViewController *citiesViewController=[[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    citiesViewController.title=@"Global Secure";
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:citiesViewController];
    navController.navigationBar.tintColor=[UIColor lightGrayColor];
    navController.title=@"Global Secure";
    
    UIImage *image = [UIImage imageNamed:@"btn_global.png"];
    UIImage *scaledImage = [image scaleToSize:CGSizeMake(80.0f, 44.0f)];    
    navController.tabBarItem.image=scaledImage;
    
    
    navController.navigationItem.backBarButtonItem = btnLogOut;
    navController.navigationItem.hidesBackButton = YES;
    navController.navigationItem.backBarButtonItem.title = @"Log Out";
    
    
    navController.view.frame=CGRectMake(0, 0, 320, 460);
    navController.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    [navController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];

    
    
        
    
    
    ContactPageViewController *promotionViewController=[[ContactPageViewController alloc] initWithNibName:@"ContactPageViewController" bundle:nil];
    promotionViewController.title=@"Global Secure";
    UINavigationController *tnavController2=[[UINavigationController alloc] initWithRootViewController:promotionViewController];
    tnavController2.navigationBar.tintColor=[UIColor lightGrayColor];
    tnavController2.title=@"Contact";
    tnavController2.view.frame=CGRectMake(0, 0, 320, 460);
    tnavController2.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    //tnavController2.tabBarItem.image=[UIImage imageNamed:@"btn_contact_lite.png"];    
    UIImage *imagecontact = [UIImage imageNamed:@"btn_contact.png"];
    UIImage *scaledImagecontact = [imagecontact scaleToSize:CGSizeMake(80.0f, 44.0f)];
    tnavController2.tabBarItem.image=scaledImagecontact;
    [tnavController2.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    
    /*UIButton* buttonAlert = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonAlert.tag = 1;
    buttonAlert.frame = CGRectMake(0, 0, 55, 30);
    buttonAlert.titleLabel.text=@"Home";
    [buttonAlert setBackgroundImage:[UIImage imageNamed:@"btn_homer.png"]  forState:UIControlStateNormal];
    [buttonAlert addTarget:self action:@selector(BtnAlertTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnAlert = [[UIBarButtonItem alloc] initWithCustomView:buttonAlert];  
    //self.navigationItem.title=@"Risk Control";
    self.navigationItem.leftBarButtonItem=btnAlert;*/
    
    FeedbackViewController *reservationViewController=[[FeedbackViewController alloc] initWithNibName:@"FeedbackViewController" bundle:nil];
    reservationViewController.title=@"Global Secure";
    UINavigationController *tnavController3=[[UINavigationController alloc] initWithRootViewController:reservationViewController];
    tnavController3.navigationBar.tintColor=[UIColor lightGrayColor];
    tnavController3.title=@"Reference";
    tnavController3.view.frame=CGRectMake(0, 0, 320, 460);
    tnavController3.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    UIImage *imageFeedback = [UIImage imageNamed:@"btn_feedback.png"];
    UIImage *scaledImagefeedback = [imageFeedback scaleToSize:CGSizeMake(80.0f, 44.0f)];
    tnavController3.tabBarItem.image=scaledImagefeedback;
    [tnavController3.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    
    
    
    AboutViewController *aboutViewController=[[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
    aboutViewController.title=@"Global Secure";
    UINavigationController *tboutnavController3=[[UINavigationController alloc] initWithRootViewController:aboutViewController];
    tboutnavController3.navigationBar.tintColor=[UIColor lightGrayColor];
    tboutnavController3.title=@"About";
    tboutnavController3.view.frame=CGRectMake(0, 0, 320, 460);
    tboutnavController3.navigationBar.barStyle=UIBarStyleBlackTranslucent;
    UIImage *imageabout = [UIImage imageNamed:@"btn_about.png"];
    UIImage *scaledImageabout = [imageabout scaleToSize:CGSizeMake(80.0f, 44.0f)];
    tboutnavController3.tabBarItem.image=scaledImageabout;
    [tboutnavController3.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    /*AboutViewController *bookingViewController=[[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
    bookingViewController.title=@"My Account";
    bookingViewController.isUsedAsMyAccountViewController=YES;
    bookingViewController.tabBarItem.image=[UIImage imageNamed:@"tabbar_btn_myaccount.png"];*/
    
    /**/NSArray *viewControllers=[NSArray arrayWithObjects:navController,tnavController2,tnavController3, tboutnavController3, nil];
    [citiesViewController release];
    self.navController1=navController;
    self.navController2=tnavController2;
    self.navController3=tnavController3;
    self.navController4=tboutnavController3;
    
    [navController release];
    [tnavController2 release];
    [tnavController3 release];
    [tboutnavController3 release];
    
    ttabBarController.viewControllers=viewControllers;
    ttabBarController.view.frame=CGRectMake(0, 0, 320, 460);
    self.tabBarController=ttabBarController;
    self.tabBarController.delegate=self;
    
    [ttabBarController release];
    
    
    //NSArray *viewControllersNavi=[NSArray arrayWithObjects:navController,tnavController2,tnavController3, tboutnavController3, nil];
    
    
    
    [self.view addSubview:self.tabBarController.view];
    //[self.navigationController popToViewController:ttabBarController animated:YES];
    
    
    
    //self.mainScreenViewController=tmainScreenViewController;
    //[tmainScreenViewController release];
    //[self.imgLoading removeFromSuperview];
    //[self.view addSubview:self.mainNavigationController.view];
    
    //[self.view addSubview:self.mainScreenViewController.view];
    
    //[self.view addSubview:navmainController.view];
    //[self.view addSubview:navmainController.view];
    //[self.view addSubview:self.mainScreenViewController.view];
    //[self.navigationController popToViewController:tmainScreenViewController animated:YES];
    //[];
    //[menuViewController.navigationController pushViewController:newOrderCategoryViewController animated:YES];
    

}

-(void)LoginPage
{
    /*LoginViewController *tmainScreenViewController=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
     //[self.view addSubview:tmainScreenViewController.view];
     //[self.navigationController popToViewController:tmainScreenViewController animated:YES];
     //[self.view addSubview:tmainScreenViewController.view];
     [self.navigationController pushViewController:tmainScreenViewController animated:YES];
     [tmainScreenViewController release];
    
    testClasssViewController *GtestClasssViewController=[[testClasssViewController alloc] initWithNibName:@"testClasssViewController"  bundle:nil];
    
    for (UIView* uiview in self.view.subviews) {
        [uiview removeFromSuperview];
    }
    [self.view addSubview:GtestClasssViewController.view];*/

    LoginViewController *GStartPageController=[[LoginViewController alloc] initWithNibName:@"LoginViewController"  bundle:nil];
    //for (UIView* uiview in self.view.subviews) {
    //    [uiview removeFromSuperview];
    //}
    [self.view addSubview:GStartPageController.view];
    //[self presentModalViewController:GStartPageController animated:YES];
}

-(void) BtnAlertTapped : (id) sender
{
    
    UIButton *clicked = (UIButton *) sender;
    NSLog(@"%d",clicked.tag);//Here you know which button has pressed
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://mobilesolutions.com.my/AA/login.php"]];
    
}

- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

-(void)initializeAfterViewLoad
{
    [self performSelector:@selector(LoginPage) withObject:nil afterDelay:2];
}

-(void)dealloc
{
    [mainNavigationController release];
    [tabBarController release];
    [navController1 release];
    [navController2 release];
    [navController3 release];
    [navController4 release];
    [mainScreenViewController release];
    [super dealloc];
}

@end







