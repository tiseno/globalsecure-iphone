//
//  ContactPageViewController.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebPageViewController.h"
#import <MessageUI/MessageUI.h>
#import "lblnameViewController.h"
#import "lblContactViewController.h"
#import "lblemailViewController.h"
#import "lblremarkViewController.h"
#import "NetworkHandler.h"
#import "Feedback.h"
#import "FeedbackRequest.h"
#import "FeedbackResponse.h"
#import "Reachability.h"

#define kConfirmingAlertTag 0
#define kConfirmedAlertTag 1

#define kConfirmingLogoutTag 3

@interface ContactPageViewController : UIViewController<UIAlertViewDelegate,MFMailComposeViewControllerDelegate, NetworkHandlerDelegate>{
    
}
@property (retain, nonatomic) NSString *memberID;
-(IBAction)btncallmass;
-(IBAction)btncallsg;
-(IBAction)btnmail;

@property (retain, nonatomic) IBOutlet UILabel *lblname;
@property (retain, nonatomic) IBOutlet UILabel *lblcontact;
@property (retain, nonatomic) IBOutlet UILabel *lblemail;
@property (retain, nonatomic) IBOutlet UILabel *lblremark;

-(IBAction)lblnametapped:(id)sender;
-(IBAction)lblcontacttapped:(id)sender;
-(IBAction)lblemailtapped:(id)sender;
-(IBAction)lblremarktapped:(id)sender;
-(IBAction)btnsubmit:(id)sender;
@property (nonatomic,retain) NSArray *fArr;

@end
