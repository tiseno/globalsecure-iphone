//
//  LoginViewController.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebPageViewController.h"
#import "MainViewController.h"
#import "ContactPageViewController.h"
#import "FeedbackViewController.h"
#import "AboutViewController.h"
#import "UIImage.h"
#import "GlobalSecureAppDelegate.h"
#import "login.h"
#import "GSinfoGeneralTypeViewController.h"

@interface LoginViewController : UIViewController<UITabBarControllerDelegate,UINavigationBarDelegate,UINavigationControllerDelegate, UITextFieldDelegate>{
    
    LoginViewController *mainScreenViewController;
    int heightOfEditedView;
    int heightOffset;
    UITextField *txtMemberID;
    BOOL iskeyboardDisplayed;
    BOOL checkLogin;
    
}

@property (nonatomic, retain) LoginViewController *mainScreenViewController;
@property (nonatomic, retain) UINavigationController *mainNavigationController;
@property (nonatomic, retain) UITabBarController *tabBarController;
@property (nonatomic, retain) UINavigationController *navController1;
@property (nonatomic, retain) UINavigationController *navController2;
@property (nonatomic, retain) UINavigationController *navController3;
@property (nonatomic, retain) UINavigationController *navController4;

-(IBAction)LoginTapped;
@property (retain, nonatomic) IBOutlet UITextField *txtMemberID;
@property (retain, nonatomic) IBOutlet UISwitch *rememberSwitch;
@property (retain, nonatomic) NSString *memberID;

@end
