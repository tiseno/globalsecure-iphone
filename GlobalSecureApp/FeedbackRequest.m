//
//  FeedbackRequest.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FeedbackRequest.h"

@implementation FeedbackRequest
@synthesize Feedback;

-(id)initWithsState:(Feedback *)iFeedback
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"getFeedback";
        self.requestType=WebserviceRequest;
        self.Feedback=iFeedback;
    }
    return self;
}


-(void)dealloc
{
    [Feedback release];
    [super dealloc];
}

-(NSString*) generateHTTPPostMessage
{
    
    NSString *postMsg = [NSString stringWithFormat:@"name=%@&contact=%@&email=%@&remark=%@",self.Feedback.Name,self.Feedback.Contact, self.Feedback.Email, self.Feedback.Remark];
    return postMsg;
} 

@end
