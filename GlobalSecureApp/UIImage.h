//
//  UIImage.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (scale)

-(UIImage*)scaleToSize:(CGSize)size;

@end