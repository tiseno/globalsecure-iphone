//
//  lblnameViewController.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AboutViewController.h"

@class ContactPageViewController;
@interface lblnameViewController : UIViewController
{
    int heightOfEditedView;
    int heightOfEditedArea;
    int heightOffset;
}

@property (nonatomic, retain) ContactPageViewController *gContactPageViewController;

@property (retain, nonatomic) IBOutlet UITextView *lblname;
-(IBAction)Donetapped:(id)sender;

@end
