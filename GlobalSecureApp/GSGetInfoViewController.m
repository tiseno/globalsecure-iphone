//
//  GSGetInfoViewController.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GSGetInfoViewController.h"

@interface GSGetInfoViewController ()

@end

@implementation GSGetInfoViewController
@synthesize tblinfo;
@synthesize infArr, GType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
    [GType release];
    [infArr release];
    [tblinfo release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    GSGetInfoViewController *gBpNewAppointmentViewController=[[GSGetInfoViewController alloc] initWithNibName:@"GSGetInfoViewController" bundle:nil];
    /**/UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [leftButton setImage:[UIImage imageNamed:@"btn_back2.png"] forState:UIControlStateNormal];  
    //leftButton.titleLabel.text=@"back";
    leftButton.frame = CGRectMake(0, 0, 50, 33);
    [leftButton addTarget:self action:@selector(handleBack:) forControlEvents:UIControlEventTouchUpInside];   
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: leftButton] autorelease]; 
    gBpNewAppointmentViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    [leftButton release];
    [gBpNewAppointmentViewController release];
    
    [self getinfo];
}

-(void)getinfo
{
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    infoRequest *requestset= [[infoRequest alloc] initWithsState:self.GType];
    
    [networkHandler setDelegate:self];
    [networkHandler request:requestset];
    [requestset release];
    [networkHandler release];
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    
    // make sure you do this!
    // pop the controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [self setTblinfo:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[getInfoResponse class]])
    {
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((getInfoResponse*)responseMessage).infoArr;
        self.infArr=msgArr;
        
        for (GSinfo *item in msgArr) {
            
            NSLog(@"item.GeneralType--->%@",item.GeneralType);

        }

        [tblinfo reloadData];

    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [self.infArr count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath {
    cell = (infoCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    
    
    //static NSString *CellIdentifier = @"Cell";
    //BpEventCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell = [[infoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    GSinfo* gBpEventcell=[self.infArr objectAtIndex:indexPath.row];
    

    cell.gGSinfo=gBpEventcell;
    cell.lblTitle.text=gBpEventcell.name;
    
    NSString *Address=[[NSString alloc]initWithFormat:@"%@%@%@ %@",gBpEventcell.add1, gBpEventcell.add2, gBpEventcell.add3, gBpEventcell.city];
    
    
    /*===========*/
    cell.imageURL.frame = CGRectMake(250, 60, 60, 40);
    
    CGSize maxsize = CGSizeMake(220, 9999);
    UIFont *thefont = [UIFont fontWithName:@"Helvetica" size:12];
    CGSize textsize = [Address sizeWithFont:thefont constrainedToSize:maxsize lineBreakMode:UILineBreakModeWordWrap];
    
    cell.Address.numberOfLines=20;
    cell.Address.frame=CGRectMake(20, 50, 220, textsize.height+50);
    //cell.city.frame=CGRectMake(20, textsize.height+65, 220, textsize.height+50);
    cell.lblphone.frame=CGRectMake(20, textsize.height+80, 280, textsize.height+50);
    cell.phone.frame=CGRectMake(65, textsize.height+80, 280, textsize.height+50);
    cell.lblfax.frame=CGRectMake(20, textsize.height+100, 280, textsize.height+50);
    cell.fax.frame=CGRectMake(65, textsize.height+100, 280, textsize.height+50);
    
    //cell.website.numberOfLines=3;
    
    cell.website.frame=CGRectMake(20, textsize.height+120, 280, textsize.height+50);
    cell.email.frame=CGRectMake(20, textsize.height+140, 280, textsize.height+50);
    
    
    cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, 320, textsize.height+220);
    
    cell.Address.text=Address;
    //cell.city.text=gBpEventcell.city;
    cell.lblphone.text=@"Phone: ";
    cell.phone.text=gBpEventcell.phone;
    cell.lblfax.text=@"Fax: ";
    cell.fax.text=gBpEventcell.fax;
    cell.website.text=gBpEventcell.website;
    cell.email.text=gBpEventcell.email;
    
    //NSString *imagename=[[NSString alloc]initWithFormat:@"http://203.106.245.234:100/images/healthtips/%@",classBpHealthTips.healthtipsImgPath];
    
    //cell.imgpicture.imageURL = [NSURL URLWithString:imagename];
    
    cell.imageURL.imageURL=[NSURL URLWithString:gBpEventcell.imageURL];
    /*===========*/
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}


@end
