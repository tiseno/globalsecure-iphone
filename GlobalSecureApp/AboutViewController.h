//
//  AboutViewController.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebPageViewController.h"

#define kConfirmingAlertTag 0
#define kConfirmedAlertTag 1
#define kConfirmingLogoutTag 3

@interface AboutViewController : UIViewController{
    
}

@property (retain, nonatomic) NSString *memberID;

@end
