//
//  GSGetInfoViewController.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkHandler.h"
#import <QuartzCore/QuartzCore.h>
#import "GSinfo.h"
#import "infoRequest.h"
#import "getInfoResponse.h"
#import "infoCell.h"
#import "AsyncImageView.h"

@interface GSGetInfoViewController : UIViewController<NetworkHandlerDelegate>
{
    infoCell *cell;
}

@property (nonatomic,retain) NSString *GType;
@property (nonatomic,retain) NSArray *infArr;
@property (retain, nonatomic) IBOutlet UITableView *tblinfo;


@end
