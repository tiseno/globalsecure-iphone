//
//  GSinfoGeneralTypeViewController.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GSinfoGeneralTypeViewController.h"

@interface GSinfoGeneralTypeViewController ()

@end

@implementation GSinfoGeneralTypeViewController
@synthesize generalArr;
@synthesize tblGeneralTypes;
@synthesize memberID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)dealloc
{
    [memberID release];
    [generalArr release];
    [tblGeneralTypes release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    GlobalSecureAppDelegate *GSapp=(GlobalSecureAppDelegate*)[UIApplication sharedApplication].delegate;
    
    //GSapp.currentFeedback=feedback;
    //lblnametxt.text=feedback.Name;
    //[lblnametxt release];
    //NSLog(@"naame--->%@",feedback.Name);
   //NSLog(@"naame--->Hello~");
    
    UIButton* buttonLogoutTapped = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLogoutTapped.tag = 1;
    buttonLogoutTapped.frame = CGRectMake(0, 0, 63, 32);
    buttonLogoutTapped.titleLabel.text=@"Home";
    [buttonLogoutTapped setBackgroundImage:[UIImage imageNamed:@"btn_logout.png"]  forState:UIControlStateNormal];
    [buttonLogoutTapped addTarget:self action:@selector(LogoutMethod:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnLogOut = [[UIBarButtonItem alloc] initWithCustomView:buttonLogoutTapped];  
    self.navigationItem.leftBarButtonItem.title = @"Log Out";
    self.navigationItem.rightBarButtonItem = btnLogOut;
    self.navigationItem.leftBarButtonItem = nil;
    
    UIButton* buttonAlert = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonAlert.tag = 1;
    buttonAlert.frame = CGRectMake(0, 0, 63, 32);
    buttonAlert.titleLabel.text=@"Home";
    [buttonAlert setBackgroundImage:[UIImage imageNamed:@"btn_alert.png"]  forState:UIControlStateNormal];
    [buttonAlert addTarget:self action:@selector(BtnAlertTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnAlert = [[UIBarButtonItem alloc] initWithCustomView:buttonAlert];  
    //self.navigationItem.title=@"Risk Control";
    self.navigationItem.leftBarButtonItem=btnAlert;

    tblGeneralTypes.layer.cornerRadius=10;
    tblGeneralTypes.layer.borderColor = [UIColor grayColor].CGColor;
    tblGeneralTypes.layer.borderWidth = 1;
    
    [self getGeneralType];
}

-(void) LogoutMethod : (id) sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm to Logout ?" message:@"" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=kConfirmingLogoutTag;
    [alert show];
    [alert release];
    
}

-(void) BtnAlertTapped : (id) sender
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Open in Safari." message:@"" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=kConfirmingAlertTag;
    [alert show];
    [alert release];
    
    
    UIButton *clicked = (UIButton *) sender;
    NSLog(@"%d",clicked.tag);//Here you know which button has pressed
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://mobilesolutions.com.my/AA/login.php"]];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://mobilesolutions.com.my/AA/login.php?memberno=GS00001SG"]];
    
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == kConfirmingAlertTag)
    {
        if(buttonIndex == 1)
        {
            
            NSString *url=[[NSString alloc]initWithFormat:@"http://mobilesolutions.com.my/AA/login.php?memberno=%@",self.memberID];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            
            
        }
    }else if(alertView.tag == kConfirmedReffTag) 
    {
        if(buttonIndex == 1)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://aa-international.com/index.php?mid=22"]];
        }
        
    }else if(alertView.tag == kConfirmingLogoutTag) 
    {
        if(buttonIndex == 1)
        {
            self.memberID=Nil;
            LoginViewController *GStartPageController=[[LoginViewController alloc] initWithNibName:@"LoginViewController"  bundle:nil];
            for (UIView* uiview in self.view.subviews) {
                [uiview removeFromSuperview];
            }
            [self.view addSubview:GStartPageController.view];
            [self presentModalViewController:GStartPageController animated:YES];
            
        }
        
    }
    
    
}

-(void)getGeneralType
{
    NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
    GSGeneralTypesRequest *requestset= [[GSGeneralTypesRequest alloc] init];
    
    [networkHandler setDelegate:self];
    [networkHandler request:requestset];
    [requestset release];
    [networkHandler release];

}

-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    
    if([responseMessage isKindOfClass:[GSGeneralTypesResponse class]])
    {
        
        
        
        NSArray *msgArr=[[[NSArray alloc]init]autorelease];
        msgArr=((GSGeneralTypesResponse*)responseMessage).GeneralTypesArr;
        self.generalArr=msgArr;
        
        for (GeneralTypes *item in msgArr) {
            
            NSLog(@"item.GeneralType--->%@",item.GeneralType);

            
            
        }
        [tblGeneralTypes reloadData];
        
        
    }
    
}

- (void)viewDidUnload
{
    [self setTblGeneralTypes:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.generalArr.count;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell.
    GeneralTypes * sushiName = [self.generalArr objectAtIndex:indexPath.row];
    //NSString *sushiString = [[NSString alloc] initWithFormat:@"%d: %@", indexPath.row, sushiName];
    NSString *sushiString = [[NSString alloc] initWithFormat:@" %@", sushiName.GeneralType];
    cell.textLabel.text = sushiString;
    [sushiString release];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GeneralTypes * sushiName = [self.generalArr objectAtIndex:indexPath.row];
    NSString * sushiString = [NSString stringWithFormat:@"%@", sushiName.GeneralType];    
    NSString * message = [NSString stringWithFormat:@" %@", sushiString];
    
    /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Selected State" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertView show];
    [alertView release];*/
    
    
    GSGetInfoViewController *gBpChooseStateViewController=[[GSGetInfoViewController alloc] initWithNibName:@"GSGetInfoViewController" bundle:nil];
    
    gBpChooseStateViewController.title=sushiString;
    gBpChooseStateViewController.GType=sushiString;
    [self.navigationController pushViewController:gBpChooseStateViewController animated:YES];
    [gBpChooseStateViewController release];
    
    
    /*NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
     BpOutletRequest *loginset= [[BpOutletRequest alloc] initWithsState:sushiString];
     
     [networkHandler setDelegate:self];
     [networkHandler request:loginset];
     [loginset release];
     [networkHandler release];*/
    
    
    //_lastSushiSelected = sushiString;
    //SelectedState = [sushiString retain];
    
}
@end
