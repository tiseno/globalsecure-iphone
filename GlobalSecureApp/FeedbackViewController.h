//
//  FeedbackViewController.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldEditViewController.h"
#import "Feedback.h"
#import "WebPageViewController.h"

#define kConfirmingAlertTag 0
#define kConfirmedAlertTag 1

#define kConfirmingReffTag 2
#define kConfirmedReffTag 4

#define kConfirmingLogoutTag 3


@interface FeedbackViewController : UIViewController{
    
}
@property (retain, nonatomic) Feedback *feedback;
@property (retain, nonatomic) IBOutlet UILabel *lblnametxt;
@property (retain, nonatomic) IBOutlet UILabel *lblcontacttxt;
@property (retain, nonatomic) IBOutlet UILabel *lblemailtxt;
@property (retain, nonatomic) IBOutlet UILabel *lblremarktxt;
-(IBAction)nametapped:(id)sender;

@property (retain, nonatomic) NSString *memberID;

@end
