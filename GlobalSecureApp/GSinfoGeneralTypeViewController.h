//
//  GSinfoGeneralTypeViewController.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalSecureAppDelegate.h"
#import "NetworkHandler.h"
#import "GeneralTypes.h"
#import "GSGeneralTypesRequest.h"
#import "GSGeneralTypesResponse.h"
#import <QuartzCore/QuartzCore.h>

#import "LoginViewController.h"
#import "GSGetInfoViewController.h"

@interface GSinfoGeneralTypeViewController : UIViewController<NetworkHandlerDelegate>{
    
}

@property (nonatomic,retain) NSArray *generalArr;
@property (retain, nonatomic) IBOutlet UITableView *tblGeneralTypes;
@property (retain, nonatomic) NSString *memberID;

@end
