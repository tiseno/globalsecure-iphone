//
//  Feedback.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Feedback : NSObject{
    
}
@property (nonatomic,retain) NSString *Name;
@property (nonatomic,retain) NSString *Contact;
@property (nonatomic,retain) NSString *Email;
@property (nonatomic,retain) NSString *Remark;
@property (nonatomic,retain) NSString *Status;

@end
