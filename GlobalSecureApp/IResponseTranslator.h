//
//  IResponseTranslator.h
//  Holiday Villa
//
//  Created by Jermin Bazazian on 4/19/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLResponse.h"

@protocol IResponseTranslator <NSObject>
-(XMLResponse*)translate:(NSData*) xmlData;
@end
