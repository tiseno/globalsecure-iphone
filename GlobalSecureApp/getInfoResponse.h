//
//  getInfoResponse.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLResponse.h"

@interface getInfoResponse : XMLResponse{
    
}
@property (retain, nonatomic) NSArray *infoArr;
@end
