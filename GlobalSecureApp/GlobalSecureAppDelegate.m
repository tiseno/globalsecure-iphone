//
//  GlobalSecureAppDelegate.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GlobalSecureAppDelegate.h"

#import "GlobalSecureViewController.h"

@implementation GlobalSecureAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;
@synthesize currentFeedback;

- (void)dealloc
{
    [currentFeedback release];
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    self.viewController = [[[GlobalSecureViewController alloc] initWithNibName:@"GlobalSecureViewController" bundle:nil] autorelease];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(NSArray*)loadProducts
{ 
    DBlogin *dataItem=[[DBlogin alloc] init];
    NSArray *items=[dataItem selectItem];
    //NSLog(@"items--->%@",items);

    [dataItem release];
     
    return items;
}

-(void)insertlogin:(login*)tlogin
{ 
    //login *loginarr=[[login alloc]init];
    //loginarr.member_id=@"ccvvbb";
    //loginarr.remember_id=1;

    DBlogin *dataItem=[[DBlogin alloc] init];
    [dataItem insertItem:tlogin];
    //[loginarr release];
    [dataItem release];
    
}

-(void)updatelogin:(login*)tlogin
{ 
    DBlogin *dataItem=[[DBlogin alloc] init];
    [dataItem updateLogin:tlogin];
    [dataItem release];
}

/*-(NSDictionary*)loadProducts
{ 
    DBlogin *dataItem=[[DBlogin alloc] init];
    NSArray *items=[dataItem selectItem];
    //[self fillItemCateogryDictionaryWithItemArr:items];
    [dataItem release];
    return items;// self.itemCategoryDictionary;
}*/


@end
