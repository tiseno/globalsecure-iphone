//
//  GSinfo.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GSinfo : NSObject{
    
}

@property (nonatomic, retain) NSString *GeneralType;

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *add1;
@property (nonatomic, retain) NSString *add2;
@property (nonatomic, retain) NSString *add3;

@property (nonatomic, retain) NSString *city;
@property (nonatomic, retain) NSString *phone;
@property (nonatomic, retain) NSString *fax;

@property (nonatomic, retain) NSString *website;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *operatehour;
@property (nonatomic, retain) NSString *imageURL;

@end



