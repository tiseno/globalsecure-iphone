//
//  DBlogin.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBbase.h"
#import "login.h"

@interface DBlogin : DBbase{
    BOOL runBatch;
}
@property (nonatomic)BOOL runBatch;
-(DataBaseInsertionResult)insertItem:(login*)tlogin;
-(DataBaseUpdateResult)updateLogin:(login*)tlogin;
-(NSArray*)selectItem;
@end
