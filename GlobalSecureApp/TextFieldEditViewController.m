//
//  TextFieldEditViewController.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TextFieldEditViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface TextFieldEditViewController ()

@end

@implementation TextFieldEditViewController
@synthesize EditTextView,feedback;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIButton* buttonLogoutTapped = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLogoutTapped.tag = 1;
    buttonLogoutTapped.frame = CGRectMake(0, 0, 55, 30);
    buttonLogoutTapped.titleLabel.text=@"Home";
    [buttonLogoutTapped setBackgroundImage:[UIImage imageNamed:@"btn_done.png"]  forState:UIControlStateNormal];
    [buttonLogoutTapped addTarget:self action:@selector(LogoutMethod:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnLogOut = [[UIBarButtonItem alloc] initWithCustomView:buttonLogoutTapped];  
    //UIBarButtonItem *toggleButton = [[UIBarButtonItem alloc] initWithTitle:@"Call" style:UIBarButtonItemStyleBordered target:roomsViewController action:@selector(callHotel:)];
    self.navigationItem.rightBarButtonItem = btnLogOut;
    
    
    
    
    [[EditTextView layer] setBorderWidth:1];
	[[EditTextView layer] setCornerRadius:15];
}

-(void) LogoutMethod : (id) sender
{
    
    UIButton *clicked = (UIButton *) sender;
    NSLog(@"%d",clicked.tag);//Here you know which button has pressed
    
    GlobalSecureAppDelegate *GSapp=(GlobalSecureAppDelegate*)[UIApplication sharedApplication].delegate;
    
    /**/UIViewController* newOrderViewController=nil;
    for(UIViewController* viewController in self.navigationController.viewControllers)
    {
        if([viewController isKindOfClass:[FeedbackViewController class]])
        {
            newOrderViewController=viewController;
            break;
        }
    }
    if(newOrderViewController!=nil)
    {
        [self.navigationController popToViewController:newOrderViewController animated:YES];
        NSString *nametxt=[[NSString alloc]initWithFormat:@"%@",self.EditTextView.text];
        GSapp.currentFeedback.Name=nametxt;
        
        NSLog(@"currentFeedback--->%@",GSapp.currentFeedback.Name);
        NSLog(@"nametxt--->%@",nametxt);
        NSLog(@"naame3--->%@",feedback.Name);
    }
    else
    {
        
        UIViewController* menuViewController=nil;
        for(UIViewController* viewController in self.navigationController.viewControllers)
        {
            if([viewController isKindOfClass:[TextFieldEditViewController class]])
            {
                menuViewController=viewController;
                break;
            }
        }
        [self.navigationController popToViewController:menuViewController animated:YES];
        FeedbackViewController *newOrderCategoryViewController=[[FeedbackViewController alloc] initWithNibName:@"FeedbackViewController" bundle:nil];
        
        NSString *nametxt=[[NSString alloc]initWithFormat:@"%@",self.EditTextView.text];
        feedback.Name=nametxt;
        
        newOrderCategoryViewController.feedback=feedback;
        NSLog(@"naame2--->%@",feedback.Name);
        [menuViewController.navigationController pushViewController:newOrderCategoryViewController animated:YES];
        [newOrderCategoryViewController release];
        
    }

    /*
    FeedbackViewController *tmainScreenViewController=[[FeedbackViewController alloc] initWithNibName:@"FeedbackViewController" bundle:nil];
    
    //[[self navigationController] setNavigationBarHidden:YES animated:YES];
    self.navigationItem.rightBarButtonItem=nil;

    [self.navigationController pushViewController:tmainScreenViewController animated:YES];
    [tmainScreenViewController release];*/
    
}


- (void)viewDidUnload
{
    [self setEditTextView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [feedback release];
    [EditTextView release];
    [super dealloc];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    /*heightOfEditedView = textView.frame.size.height;
    heightOffset = textView.frame.origin.y+10;
    
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];*/
    
    
    
    
}

@end
