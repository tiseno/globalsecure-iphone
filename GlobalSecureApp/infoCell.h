//
//  infoCell.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSinfo.h"

@interface infoCell : UITableViewCell{
    
}

@property (nonatomic, retain) UIColor *lineColor;
@property (nonatomic, retain) UILabel *lblTitle;
@property (nonatomic, retain) UILabel *lblname;

@property (nonatomic, retain) UILabel *lblAddress;
@property (nonatomic, retain) UILabel *Address;

@property (nonatomic, retain) UILabel *lblcity;
@property (nonatomic, retain) UILabel *city;

@property (nonatomic, retain) UILabel *lblfax;
@property (nonatomic, retain) UILabel *fax;

@property (nonatomic, retain) UILabel *lblphone;
@property (nonatomic, retain) UILabel *phone;

@property (nonatomic, retain) UILabel *lblwebsite;
@property (nonatomic, retain) UILabel *website;

@property (nonatomic, retain) UILabel *lblemail;
@property (nonatomic, retain) UILabel *email;

@property (nonatomic, retain) UILabel *lbloperatehour;
@property (nonatomic, retain) UILabel *operatehour;

@property (nonatomic, retain) UIImageView *imageURL;

@property (nonatomic, retain) GSinfo *gGSinfo;


@end


/* <GeneralType>string</GeneralType>
 <name>string</name>
 <add1>string</add1>
 <add2>string</add2>
 <add3>string</add3>
 <city>string</city>
 <phone>string</phone>
 <fax>string</fax>
 <website>string</website>
 <email>string</email>
 <operatehour>string</operatehour>
 <imageURL>string</imageURL>*/
