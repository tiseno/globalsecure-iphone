//
//  TextFieldEditViewController.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedbackViewController.h"
#import "Feedback.h"
#import "GlobalSecureAppDelegate.h"

@interface TextFieldEditViewController : UIViewController<UITextViewDelegate>{
    
    int heightOfEditedView;
    int heightOffset;
}
@property (retain, nonatomic) IBOutlet UITextView *EditTextView;
@property (retain, nonatomic) Feedback *feedback;

@end
