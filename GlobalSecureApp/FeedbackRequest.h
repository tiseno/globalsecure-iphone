//
//  FeedbackRequest.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLRequest.h"
#import "Feedback.h"

@interface FeedbackRequest : XMLRequest{
    
}

@property (nonatomic, retain) Feedback *Feedback;

-(id)initWithsState:(Feedback *)iFeedback;

@end
