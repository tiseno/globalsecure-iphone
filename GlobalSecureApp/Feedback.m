//
//  Feedback.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Feedback.h"

@implementation Feedback
@synthesize Name,Contact,Email,Remark;

-(void)dealloc
{
    [Remark release];
    [Email release];
    [Contact release];
    [Name release];
    [super dealloc];
    
}

@end
