//
//  AboutViewController.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController
@synthesize memberID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton* buttonLogoutTapped = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLogoutTapped.tag = 1;
    buttonLogoutTapped.frame = CGRectMake(0, 0, 63, 32);
    buttonLogoutTapped.titleLabel.text=@"Home";
    [buttonLogoutTapped setBackgroundImage:[UIImage imageNamed:@"btn_logout.png"]  forState:UIControlStateNormal];
    [buttonLogoutTapped addTarget:self action:@selector(LogoutMethod:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnLogOut = [[UIBarButtonItem alloc] initWithCustomView:buttonLogoutTapped];  
    self.navigationItem.leftBarButtonItem.title = @"Log Out";
    self.navigationItem.rightBarButtonItem = btnLogOut;
    self.navigationItem.leftBarButtonItem = nil;
    
    UIButton* buttonAlert = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonAlert.tag = 1;
    buttonAlert.frame = CGRectMake(0, 0, 63, 32);
    buttonAlert.titleLabel.text=@"Home";
    [buttonAlert setBackgroundImage:[UIImage imageNamed:@"btn_alert.png"]  forState:UIControlStateNormal];
    [buttonAlert addTarget:self action:@selector(BtnAlertTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem* btnAlert = [[UIBarButtonItem alloc] initWithCustomView:buttonAlert];  
    //self.navigationItem.title=@"Risk Control";
    self.navigationItem.leftBarButtonItem=btnAlert;
}

-(void) LogoutMethod : (id) sender
{
    
    UIButton *clicked = (UIButton *) sender;
    NSLog(@"%d",clicked.tag);//Here you know which button has pressed
    
    /*WebPageViewController *tmainScreenViewController=[[WebPageViewController alloc] initWithNibName:@"WebPageViewController" bundle:nil];
    [self.navigationController pushViewController:tmainScreenViewController animated:YES];
    [tmainScreenViewController release];*/
    //NSLog(@"%@",self.UserIDf);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm to Logout ?" message:@"" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=kConfirmingLogoutTag;
    [alert show];
    [alert release];

}


-(void) BtnAlertTapped : (id) sender
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Open in Safari." message:@"" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=kConfirmingAlertTag;
    [alert show];
    [alert release];
    
    
    UIButton *clicked = (UIButton *) sender;
    NSLog(@"%d",clicked.tag);//Here you know which button has pressed
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://mobilesolutions.com.my/AA/login.php"]];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://mobilesolutions.com.my/AA/login.php?memberno=GS00001SG"]];
    
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == kConfirmingAlertTag)
    {
        if(buttonIndex == 1)
        {
            
            NSString *url=[[NSString alloc]initWithFormat:@"http://mobilesolutions.com.my/AA/login.php?memberno=%@",self.memberID];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
            
            
            
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Cancel" message:@"The order has been deleted" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             alert.tag=kConfirmedAlertTag;
             [alert show];
             [alert release];*/
            
        }
    }else if(alertView.tag == kConfirmingLogoutTag) 
    {
        if(buttonIndex == 1)
        {
            self.memberID=Nil;
            LoginViewController *GStartPageController=[[LoginViewController alloc] initWithNibName:@"LoginViewController"  bundle:nil];
            for (UIView* uiview in self.view.subviews) {
                [uiview removeFromSuperview];
            }
            [self.view addSubview:GStartPageController.view];
            [self presentModalViewController:GStartPageController animated:YES];
            
        }
        
    }
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dealloc
{
    [memberID release];
    [super dealloc];
}

@end
