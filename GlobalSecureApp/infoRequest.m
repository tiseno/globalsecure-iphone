//
//  infoRequest.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 8/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "infoRequest.h"

@implementation infoRequest
@synthesize GeneralType;

-(id)initWithsState:(NSString *)istate
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"getInfo";
        self.requestType=WebserviceRequest;
        self.GeneralType=istate;
    }
    return self;
}


-(void)dealloc
{
    [GeneralType release];
    [super dealloc];
}

-(NSString*) generateHTTPPostMessage
{
    
    NSString *postMsg = [NSString stringWithFormat:@"GeneralType=%@",self.GeneralType];
    return postMsg;
} 

@end
