//
//  DBlogin.m
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DBlogin.h"

@implementation DBlogin
@synthesize runBatch;

-(DataBaseInsertionResult)insertItem:(login*)tlogin;
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *insertSQL;
        //NSLog(@"tlogin.member_id-->%@",tlogin.member_id);
        if([tlogin.member_id length]!=0)
        {
            //NSString *itemDescription=[tItem.Description stringByReplacingOccurrencesOfString:@"'" withString:@"\""]; 
            //NSString *itemNo=[tItem.Item_No stringByReplacingOccurrencesOfString:@"'" withString:@"\""];
            
            insertSQL = [NSString stringWithFormat:@"insert into tblLogin(memberid, rememberid) values('%@','%d');",tlogin.member_id,tlogin.remember_id];

            
        }
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
           
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        //NSLog(@"%@",insertSQL);
        //NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}

-(DataBaseDeletionResult)deletItem:(login*)tlogin
{
    NSString *insertSQL = [NSString stringWithFormat:@"delete from tblLogin where id=1;"];
    sqlite3_stmt *statement;
    const char *insert_stmt = [insertSQL UTF8String];
    
    sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
    if(sqlite3_step(statement) == SQLITE_DONE)
    {
        sqlite3_finalize(statement);
        /*if([self deletUOMAndConversionForItem:tItem]!=DataBaseDeletionSuccessful)
        {
            return DataBaseDeletionFailed;
        }
        if([self deletDefaultPriceAndUOMForItem:tItem]!=DataBaseDeletionSuccessful)
        {
            return DataBaseDeletionFailed;
        }*/
        return DataBaseDeletionSuccessful;
    }
    NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
    sqlite3_finalize(statement);
    return DataBaseDeletionFailed;
}

-(DataBaseUpdateResult)updateLogin:(login*)tlogin
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        
        NSLog(@"db tlogin.remember_id----->%d",tlogin.remember_id);
        
        NSString *rememberid=[[NSString alloc]initWithFormat:@"%d",tlogin.remember_id];
        
        NSString* updateSQL=[NSString stringWithFormat:@"update tblLogin set memberid='%@' , rememberid='%@' where id=1 ;", tlogin.member_id,rememberid];
        sqlite3_stmt *statement;
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection];
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        return DataBaseUpdateSuccessful;
    }
    else
    {
        return DataBaseUpdateFailed;
    }
    
}

/*-(NSArray*)selectItem
{
    NSMutableArray* itemarr=nil;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        itemarr=[[[NSMutableArray alloc] init] autorelease];
        NSString *selectSQL=[NSString stringWithFormat:@"select * from tblLogin where id=1"];
        
        NSLog(@"selectSQL--->%@",selectSQL);
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        login *prevItem=nil;
        int prevItemID=-1;
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            int curItemID=sqlite3_column_int(statement, 0);
            if(prevItemID!=curItemID)
            {
                login *tlogin= [[login alloc] init];
                
                tlogin.login_id=curItemID;
                
                int itemID=sqlite3_column_int(statement, 0);
                tlogin.login_id=itemID;
                
                NSString *memberid=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                tlogin.member_id = memberid;
                
                //NSString *rememberid=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
                 int rememberid=sqlite3_column_int(statement, 2);
                tlogin.remember_id = rememberid;
                
                
                
                [itemarr addObject:tlogin];
                prevItem=tlogin;
                prevItemID=curItemID;
                [tlogin release];
                
                NSLog(@"tlogin.member_id-->%@",tlogin.member_id);
                NSLog(@"tlogin.remember_id-->%@",tlogin.remember_id);
            }
            
        }
        
        [self closeConnection];
    }
    return itemarr;
}*/

-(NSArray*)selectItem
{
    NSMutableArray *itemarr = [[[NSMutableArray alloc] init] autorelease];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        //NSString *selectSQL=[NSString stringWithFormat:@"select code,name,password,username from Salesperson"];
        NSString *selectSQL=[NSString stringWithFormat:@"select id, memberid, rememberid from tblLogin"];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
                       
            
            login *tlogin= [[login alloc] init];
            
            //tlogin.login_id=curItemID;
            
            int itemID=sqlite3_column_int(statement, 0);
            tlogin.login_id=itemID;
            
            
            char* controlCStr=(char*)sqlite3_column_text(statement, 1);
            //NSLog(@"controlCStr--->%@",controlCStr);
            if(controlCStr)
            {
                //NSString *memberid=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                NSString *memberid=[NSString stringWithUTF8String:(char*)controlCStr];
                tlogin.member_id = memberid;
            }
            
            
            //NSString *rememberid=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
            int rememberid=sqlite3_column_int(statement, 2);
            tlogin.remember_id = rememberid;
            
            
            
            [itemarr addObject:tlogin];
            /*prevItem=tlogin;
            prevItemID=curItemID;;*/
            [tlogin release];
            
           

            
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}

@end
