//
//  GlobalSecureAppDelegate.h
//  GlobalSecureApp
//
//  Created by Tiseno Mac 2 on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Feedback.h"
#import "DBlogin.h"
#import "login.h"

@class GlobalSecureViewController;

@interface GlobalSecureAppDelegate : UIResponder <UIApplicationDelegate>{
    
}

@property (nonatomic, retain) Feedback* currentFeedback;
@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) GlobalSecureViewController *viewController;
-(NSArray*)loadProducts;
-(void)insertlogin:(login*)tlogin;
-(void)updatelogin:(login*)tlogin;
@end
